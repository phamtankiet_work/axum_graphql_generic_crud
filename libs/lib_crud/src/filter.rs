use serde::{Deserialize, Serialize};

use lib_utils::{time::OffsetDateTime, uuid::Uuid};

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub enum FilterOperation {
    Eq,
    Neq,
    Gt,
    Gte,
    Lt,
    Lte,
    In,
    NotIn,
    Contains,
    NotContains,
}

#[derive(Default, Debug, Clone, Serialize, Deserialize)]
pub struct FilterValue {
    pub uuid_value: Option<Uuid>,
    pub uuid_array_value: Option<Vec<Uuid>>,
    pub string_value: Option<String>,
    pub int_value: Option<i32>,
    pub bool_value: Option<bool>,
    pub string_array_value: Option<Vec<String>>,
    pub date_time_value: Option<OffsetDateTime>,
}

pub trait FilterInput {
    fn field_name_as_string(&self) -> String;
    fn operation(&self) -> FilterOperation;
    fn value(&self) -> FilterValue;
}
