use lib_utils::strum;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize, strum::Display)]
#[strum(serialize_all = "snake_case")]
pub enum SortOrder {
    Asc,
    Desc,
}
pub trait SortInput {
    fn field_name_as_string(&self) -> String;
    fn order(&self) -> SortOrder;
}

impl SortOrder {
    pub fn to_str(&self) -> &'static str {
        match self {
            SortOrder::Asc => "asc",
            SortOrder::Desc => "desc",
        }
    }
}
