use serde::{Deserialize, Serialize};

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct PaginationInput {
    pub page: i32,
    pub per_page: i32,
}
impl Default for PaginationInput {
    fn default() -> PaginationInput {
        PaginationInput {
            page: 1,
            per_page: 10,
        }
    }
}
#[derive(Debug, Serialize, Deserialize)]
pub struct PaginationResult {
    pub page: i32,
    pub per_page: i32,
    pub total_result: i32,
    pub total_pages: i32,
    pub has_previous_page: bool,
    pub has_next_page: bool,
}

impl PaginationResult {
    pub fn new(page: i32, per_page: i32, total_result: i32) -> Self {
        let total_pages = (total_result + per_page - 1) / per_page;
        Self {
            page,
            per_page,
            total_result,
            total_pages,
            has_previous_page: page > 1,
            has_next_page: page < total_pages,
        }
    }
}
