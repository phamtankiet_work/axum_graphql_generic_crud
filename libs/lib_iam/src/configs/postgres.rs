use super::{try_get_env, try_get_env_parse, Error, FetchConfig, Result};

#[derive(Debug, Clone)]
pub struct PostgresConfig {
    database_uri: String,
    max_pool_size: u32, // default: 10, range 1 - 100
    acquire_timeout_in_milisecs: u64, // default 5000 millisecs
}

impl FetchConfig for PostgresConfig {
    fn fetch_config() -> Result<Self> {
        let database_uri = try_get_env("POSTGRES_URI")?;
        let max_pool_size: u32 =
            try_get_env_parse::<u32>("POSTGRES_MAX_POOL_SIZE").unwrap_or(10);
        let acquire_timeout_in_milisecs: u64 =
            try_get_env_parse::<u64>("POSTGRES_ACQUIRE_TIMEOUT")
                .unwrap_or(5000);
        if !(1..=100).contains(&max_pool_size) {
            return Err(Error::ValueOutOfRange {
                min: 1,
                max: 100,
                actual: max_pool_size.into(),
                name: "POSTGRES_MAX_POOL_SIZE",
            });
        }

        if !(1000..=100000).contains(&acquire_timeout_in_milisecs) {
            return Err(Error::ValueOutOfRange {
                min: 1000,
                max: 100000,
                actual: acquire_timeout_in_milisecs,
                name: "POSTGRES_ACQUIRE_TIMEOUT",
            });
        }
        Ok(Self {
            database_uri,
            max_pool_size,
            acquire_timeout_in_milisecs,
        })
    }
}

impl PostgresConfig {
    pub fn database_uri(&self) -> &str {
        &self.database_uri
    }

    pub fn max_pool_size(&self) -> &u32 {
        &self.max_pool_size
    }

    pub fn acquire_timeout_in_milisecs(&self) -> &u64 {
        &self.acquire_timeout_in_milisecs
    }
}
