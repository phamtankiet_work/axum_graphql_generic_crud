use crate::database::ConnectionManager;
use lib_utils::uuid;
use sqlx::PgPool;

type PoolType = PgPool;
type IdType = uuid::Uuid;

pub trait AuthenticationService {
    fn login_by_username_and_password(
        connection_manager: &impl ConnectionManager<PoolType>,
    );
}
