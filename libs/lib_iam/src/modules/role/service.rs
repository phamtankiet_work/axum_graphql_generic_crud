use crate::database::ConnectionManager;
use lib_utils::uuid;
use sqlx::PgPool;

use super::entity::*;

type PoolType = PgPool;
type IdType = uuid::Uuid;

pub trait RoleService {
    fn find_one_by_id(
        connection_manager: &impl ConnectionManager<PoolType>,
        id: IdType,
    ) -> super::Result<Role>;
    fn create_one(
        connection_manager: &impl ConnectionManager<PoolType>,
        create_input: CreateRoleInput,
    ) -> super::Result<Role>;
    fn update_one_by_id(
        connection_manager: &impl ConnectionManager<PoolType>,
        id: IdType,
        update_input: UpdateRoleInput,
    ) -> super::Result<Role>;
    fn delete_one_by_id(
        connection_manager: &impl ConnectionManager<PoolType>,
        id: IdType,
    ) -> super::Result<Role>;
}
