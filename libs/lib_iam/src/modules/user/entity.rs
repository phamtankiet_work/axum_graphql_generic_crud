use lib_utils::time::{now_utc, OffsetDateTime};
use lib_utils::uuid::{now_v7, Uuid};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize, sqlx::FromRow)]
pub struct User {
    pub id: Option<Uuid>,
    pub name: Option<String>,
    pub created_at: Option<OffsetDateTime>,
    pub updated_at: Option<OffsetDateTime>,

    pub username: Option<String>,
    pub password: Option<String>,
}

impl User {
    pub fn fill_data_to_save(self) -> Self {
        let mut data = self.clone();
        if data.id.is_none() {
            let id: Uuid = now_v7();
            data.id = Some(id);
        }
        if data.created_at.is_none() {
            data.created_at = Some(now_utc());
        }
        if data.updated_at.is_none() {
            data.updated_at = Some(now_utc());
        }
        data
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct CreateUserInput {}

#[derive(Debug, Serialize, Deserialize)]
pub struct UpdateUserInput {}
