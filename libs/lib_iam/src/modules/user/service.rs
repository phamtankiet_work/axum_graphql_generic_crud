use crate::database::ConnectionManager;
use lib_utils::uuid;
use sqlx::PgPool;

use super::entity::*;

type PoolType = PgPool;
type IdType = uuid::Uuid;

pub trait UserService {
    fn find_one_by_id(
        connection_manager: &impl ConnectionManager<PoolType>,
        id: IdType,
    ) -> super::Result<User>;
    fn create_one(
        connection_manager: &impl ConnectionManager<PoolType>,
        create_input: CreateUserInput,
    ) -> super::Result<User>;
    fn update_one_by_id(
        connection_manager: &impl ConnectionManager<PoolType>,
        id: IdType,
        update_input: UpdateUserInput,
    ) -> super::Result<User>;
    fn delete_one_by_id(
        connection_manager: &impl ConnectionManager<PoolType>,
        id: IdType,
    ) -> super::Result<User>;
}
