pub mod error;

pub mod entity;
pub mod service;

pub use error::{Error, Result};
