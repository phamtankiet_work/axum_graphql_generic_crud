use lib_utils::time::{now_utc, OffsetDateTime};
use lib_utils::uuid::{now_v7, Uuid};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize, sqlx::FromRow)]
pub struct UserRole {
    pub user_id: Option<Uuid>,
    pub role_id: Option<Uuid>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct CreateUserRoleInput {
    pub role_id: Uuid,
    pub user_id: Uuid,
}
