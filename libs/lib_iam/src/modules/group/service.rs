use crate::database::ConnectionManager;
use lib_utils::uuid;
use sqlx::PgPool;

use super::entity::*;

type PoolType = PgPool;
type IdType = uuid::Uuid;

pub trait GroupService {
    fn find_one_by_id(
        connection_manager: &impl ConnectionManager<PoolType>,
        id: IdType,
    ) -> super::Result<Group>;
    fn create_one(
        connection_manager: &impl ConnectionManager<PoolType>,
        create_input: CreateGroupInput,
    ) -> super::Result<Group>;
    fn update_one_by_id(
        connection_manager: &impl ConnectionManager<PoolType>,
        id: IdType,
        update_input: UpdateGroupInput,
    ) -> super::Result<Group>;
    fn delete_one_by_id(
        connection_manager: &impl ConnectionManager<PoolType>,
        id: IdType,
    ) -> super::Result<Group>;
}
