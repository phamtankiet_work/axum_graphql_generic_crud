use lib_utils::time::{now_utc, OffsetDateTime};
use lib_utils::uuid::{now_v7, Uuid};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize, sqlx::FromRow)]
pub struct UserGroup {
    pub user_id: Option<Uuid>,
    pub group_id: Option<Uuid>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct CreateUserGroupInput {
    pub user_id: Uuid,
    pub group_id: Uuid,
}
