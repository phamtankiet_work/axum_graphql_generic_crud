use lib_utils::env::*;
use std::env::set_var;

#[test]
fn try_get_env_ok() {
    set_var("SOME_ENV", "value");
    try_get_env("SOME_ENV").unwrap();
}

#[test]
fn try_get_env_err() {
    let result = try_get_env("SOME_OTHER_ENV");
    assert!(result.is_err());
    let error = result.unwrap_err();
    assert!(matches!(error, Error::MissingEnv(_)));
}

#[test]
fn try_get_env_parse_ok() {
    set_var("INT_ENV", "1");
    let result: i32 = try_get_env_parse("INT_ENV").unwrap();
    assert_eq!(1, result);
}

#[test]
fn try_get_env_parse_err() {
    set_var("STRING_ENV", "string");
    let result = try_get_env_parse::<i32>("STRING_ENV");
    assert!(result.is_err());
    let error = result.unwrap_err();
    assert!(matches!(error, Error::WrongFormat(_)));
}
