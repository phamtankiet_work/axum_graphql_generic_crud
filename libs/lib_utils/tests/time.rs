use lib_utils::time::*;

#[test]
fn now_utc_ok() {
    now_utc();
}

#[test]
fn now_utc_plus_sec_str_ok() {
    now_utc_plus_sec_str(1.0);
}

#[test]
fn parse_utc_ok() {
    try_parse_utc("2024-08-30T21:32:52.52Z").unwrap();
}

#[test]
fn parse_utc_err() {
    let result = try_parse_utc("2024-08-30T21:32:52.52");
    assert!(result.is_err());
    let error = result.unwrap_err();
    assert!(matches!(error, Error::ValueIsNotInRfc3339Format(_)));
}
