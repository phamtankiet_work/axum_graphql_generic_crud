use lib_utils::uuid::*;

#[test]
fn now_v7_ok() {
    now_v7();
}

#[test]
fn try_parse_ok() {
    try_parse("f88c80c2-6748-11ef-b864-0242ac120002").unwrap();
}

#[test]
fn try_parse_err() {
    let result = try_parse("f88c80c2-6748-11ef-");
    assert!(result.is_err());
    let error = result.unwrap_err();
    assert!(matches!(error, Error::ValueIsNotInUuidFormat(_)));
}
