pub use uuid::Uuid;

pub fn now_v7() -> Uuid {
    Uuid::now_v7()
}

pub fn try_parse(uuid: &str) -> Result<Uuid> {
    Uuid::try_parse(uuid)
        .map_err(|_| Error::ValueIsNotInUuidFormat(uuid.to_string()))
}

// region:    --- Error

pub type Result<T> = core::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    ValueIsNotInUuidFormat(String),
}

// region:    --- Error Boilerplate
impl core::fmt::Display for Error {
    fn fmt(
        &self,
        fmt: &mut core::fmt::Formatter,
    ) -> core::result::Result<(), core::fmt::Error> {
        write!(fmt, "{self:?}")
    }
}

impl std::error::Error for Error {}
// endregion: --- Error Boilerplate

// endregion: --- Error
