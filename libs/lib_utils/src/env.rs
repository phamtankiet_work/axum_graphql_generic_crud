use std::env;
use std::str::FromStr;
use std::sync::Once;

use dotenvy::dotenv;

static INIT: Once = Once::new();

fn run_once() {
    INIT.call_once(|| {
        // Code to run only once
        println!("⏳ Load config from env...");
        dotenv().ok();
        println!("✅ Config load successfully");
    });
}

pub fn try_get_env(name: &'static str) -> Result<String> {
    run_once();
    env::var(name).map_err(|_| Error::MissingEnv(name))
}

pub fn try_get_env_parse<T: FromStr>(name: &'static str) -> Result<T> {
    let val = try_get_env(name)?;
    val.parse::<T>()
        .map_err(|_| Error::WrongFormat(name))
}

// region:    --- Error
pub type Result<T> = core::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    MissingEnv(&'static str),
    WrongFormat(&'static str),
}

// region:    --- Error Boilerplate
impl core::fmt::Display for Error {
    fn fmt(
        &self,
        fmt: &mut core::fmt::Formatter,
    ) -> core::result::Result<(), core::fmt::Error> {
        write!(fmt, "{self:?}")
    }
}

impl std::error::Error for Error {}
// endregion: --- Error Boilerplate

// endregion: --- Error
