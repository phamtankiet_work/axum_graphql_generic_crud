// use mongodb::{options::ClientOptions, Client};
// use tap::TapFallible;

// use crate::configs::mongodb::MongoDBConfig;

// #[derive(Clone)]
// pub struct MongoDB {
//     pub client: Client,
// }

// impl MongoDB {
//     pub async fn init(database_config: MongoDBConfig) -> Self {
//         println!("⏳ Connecting to MongoDB...");

//         let mut client_options =
//             ClientOptions::parse(database_config.uri).await.unwrap();
//         client_options.connect_timeout = database_config.connection_timeout;
//         client_options.max_pool_size = database_config.max_pool_size;
//         client_options.min_pool_size = database_config.min_pool_size;
//         // the server will select the algorithm it supports from the list provided by the driver
//         client_options.compressors = database_config.compressors;
//         let client = Client::with_options(client_options)
//             .tap_ok(|_| println!("✅ MongoDB connected successfully!"))
//             .unwrap_or_else(|err| {
//                 panic!("🔥 Failed to connect to the MongoDB: {:?}", err)
//             });

//         Self { client }
//     }
// }
