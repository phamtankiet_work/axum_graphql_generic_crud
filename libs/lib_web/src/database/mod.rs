pub mod error;
pub mod postgres;

pub use error::{Error, Result};

pub trait ConnectionManager<T> {
    fn get_connection(&self) -> T;
}
