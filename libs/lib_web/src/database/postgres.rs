use std::time::Duration;

use sqlx::{postgres::PgPoolOptions, PgPool, Pool, Postgres};

use crate::configs::PostgresConfig;

use super::{ConnectionManager, Error, Result};

pub struct PostgresManager {
    pool: PgPool,
}

impl ConnectionManager<PgPool> for PostgresManager {
    fn get_connection(&self) -> PgPool {
        self.pool.clone()
    }
}

impl PostgresManager {
    pub async fn connect(config: PostgresConfig) -> Result<Self> {
        println!("⏳ Connecting to Postgres...");
        let pool = PgPoolOptions::new()
            .max_connections(*config.max_pool_size())
            .acquire_timeout(Duration::from_secs(
                *config.acquire_timeout_in_milisecs(),
            ))
            .connect(config.database_uri())
            .await
            .map_err(|err| {
                Error::ConnectionError(format!(
                    "🔥 Failed to connect to the Postgres: {:?}",
                    err
                ))
            })?;
        println!("✅ Postgres connected successfully!");
        Ok(Self { pool })
    }

    /// Only for unit test
    #[allow(unused)]
    pub async fn create_with_pool_for_unit_test(pool: Pool<Postgres>) -> Self {
        Self { pool }
    }

    pub async fn migration(&self) -> Result<()> {
        sqlx::migrate!("./migrations")
            .run(&self.pool)
            .await
            .map_err(|err| {
                Error::MigrationError(format!("🔥 Migration error: {:?}", err))
            })?;
        println!("✅ Migration success");
        Ok(())
    }
}
