use super::super::repository_postgres::*;
use super::*;
use lib_crud::{FilterOperation, PaginationInput, SortInput, SortOrder};
use lib_utils::{time, uuid};

// region:      ======= uuid value =======
#[test]
fn build_query_for_uuid_value_unsupport_operation() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let result = RepositoryPostgres::build_query_for(
        query,
        "id".to_string(),
        FilterOperation::NotContains,
        uuid::try_parse("019146a0-ecff-7bf1-bd89-9344475cc9cb").unwrap(),
    );
    assert!(result.is_err());
    let error = result.err().unwrap();
    assert!(matches!(error, Error::FilterOperationNotSupport { .. }));
}

#[test]
fn build_query_for_uuid_value_eq_operation() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let result = RepositoryPostgres::build_query_for(
        query,
        "id".to_string(),
        FilterOperation::Eq,
        uuid::Uuid::try_parse("019146a0-ecff-7bf1-bd89-9344475cc9cb").unwrap(),
    );
    let query_str = result.unwrap().into_sql();
    assert_eq!(query_str, " AND id = $1");
}
// endregion:   ======= uuid value  =======

// region:      ======= uuid array value =======

fn create_vec_of_uuid() -> Vec<uuid::Uuid> {
    vec![
        uuid::try_parse("019146a0-ecff-7bf1-bd89-9344475cc9cb").unwrap(),
        uuid::try_parse("019146a0-ed00-7541-955c-cc6e3202a413").unwrap(),
        uuid::try_parse("019146a0-ed00-7541-955c-cc01f7f26cc5").unwrap(),
        uuid::try_parse("019146a0-ed00-7541-955c-cc4688783e78").unwrap(),
        uuid::try_parse("019146a0-ed01-7dc0-9e97-02b31bd195cc").unwrap(),
    ]
}

#[test]
fn build_query_for_uuid_array_value_unsupport_operation() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let result = RepositoryPostgres::build_query_for(
        query,
        "id".to_string(),
        FilterOperation::NotContains,
        create_vec_of_uuid(),
    );
    assert!(result.is_err());
    let error = result.err().unwrap();
    let expected_error = Error::FilterOperationNotSupport {
        value_type: "Uuid array value".to_string(),
        support_operation: vec![
            FilterOperation::In,
            FilterOperation::NotIn,
        ],
        actual_operation: FilterOperation::NotContains,
    };
    assert_eq!(error.to_string(), expected_error.to_string());
}

#[test]
fn build_query_for_uuid_array_value_in_operation() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let result = RepositoryPostgres::build_query_for(
        query,
        "id".to_string(),
        FilterOperation::In,
        create_vec_of_uuid(),
    );
    let query_str = result.unwrap().into_sql();
    assert_eq!(query_str, " AND id IN ($1, $2, $3, $4, $5) ");
}

#[test]
fn build_query_for_uuid_array_value_not_in_operation() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let result = RepositoryPostgres::build_query_for(
        query,
        "id".to_string(),
        FilterOperation::NotIn,
        create_vec_of_uuid(),
    );
    let query_str = result.unwrap().into_sql();
    assert_eq!(query_str, " AND id NOT IN ($1, $2, $3, $4, $5) ");
}

// endregion:   ======= uuid array value =======

// region:      ======= string value =======

#[test]
fn build_query_for_string_value_not_support() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let result = RepositoryPostgres::build_query_for(
        query,
        "title".to_string(),
        FilterOperation::Lte,
        "test".to_string(),
    );
    assert!(result.is_err());
    let error = result.err().unwrap();
    let expected_error = Error::FilterOperationNotSupport {
        value_type: "String value".to_string(),
        support_operation: vec![
            FilterOperation::Eq,
            FilterOperation::Neq,
            FilterOperation::Contains,
            FilterOperation::NotContains,
        ],
        actual_operation: FilterOperation::Lte,
    };
    assert_eq!(error.to_string(), expected_error.to_string())
}

#[tokio::test]
async fn build_query_for_string_value_eq_operation() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let result = RepositoryPostgres::build_query_for(
        query,
        "title".to_string(),
        FilterOperation::Eq,
        "test".to_string(),
    );
    let query_str = result.unwrap().into_sql();
    assert_eq!(query_str, " AND title = $1");
}

#[tokio::test]
async fn build_query_for_string_value_neq_operation() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let result = RepositoryPostgres::build_query_for(
        query,
        "title".to_string(),
        FilterOperation::Neq,
        "test".to_string(),
    );
    let query_str = result.unwrap().into_sql();
    assert_eq!(query_str, " AND title != $1");
}

#[tokio::test]
async fn build_query_for_string_value_contains_operation() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let result = RepositoryPostgres::build_query_for(
        query,
        "title".to_string(),
        FilterOperation::Contains,
        "test".to_string(),
    );
    let query_str = result.unwrap().into_sql();
    assert_eq!(query_str, " AND title ILIKE concat('%', $1, '%')");
}

#[tokio::test]
async fn build_query_for_string_value_not_contains_operation() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let result = RepositoryPostgres::build_query_for(
        query,
        "title".to_string(),
        FilterOperation::NotContains,
        "test".to_string(),
    );
    let query_str = result.unwrap().into_sql();
    assert_eq!(query_str, " AND title NOT ILIKE concat('%', $1, '%')");
}
// endregion:   ======= string value =======

// region:      ======= int value =======

#[test]
fn build_query_for_int_value_unsupport_operation() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let result = RepositoryPostgres::build_query_for(
        query,
        "read_count".to_string(),
        FilterOperation::NotContains,
        10,
    );
    assert!(result.is_err());
    let error = result.err().unwrap();
    let expected_error = Error::FilterOperationNotSupport {
        value_type: "Integer value".to_string(),
        support_operation: vec![
            FilterOperation::Eq,
            FilterOperation::Neq,
            FilterOperation::Gt,
            FilterOperation::Lt,
            FilterOperation::Gte,
            FilterOperation::Lte,
        ],
        actual_operation: FilterOperation::NotContains,
    };
    assert_eq!(error.to_string(), expected_error.to_string());
}

#[test]
fn build_query_for_int_value_eq_operation() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let result = RepositoryPostgres::build_query_for(
        query,
        "read_count".to_string(),
        FilterOperation::Eq,
        10,
    );
    let query_str = result.unwrap().into_sql();
    assert_eq!(query_str, " AND read_count = $1");
}

#[test]
fn build_query_for_int_value_neq_operation() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let result = RepositoryPostgres::build_query_for(
        query,
        "read_count".to_string(),
        FilterOperation::Neq,
        10,
    );
    let query_str = result.unwrap().into_sql();
    assert_eq!(query_str, " AND read_count != $1");
}

#[test]
fn build_query_for_int_value_gt_operation() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let result = RepositoryPostgres::build_query_for(
        query,
        "read_count".to_string(),
        FilterOperation::Gt,
        10,
    );
    let query_str = result.unwrap().into_sql();
    assert_eq!(query_str, " AND read_count > $1");
}

#[test]
fn build_query_for_int_value_lt_operation() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let result = RepositoryPostgres::build_query_for(
        query,
        "read_count".to_string(),
        FilterOperation::Lt,
        10,
    );
    let query_str = result.unwrap().into_sql();
    assert_eq!(query_str, " AND read_count < $1");
}

#[test]
fn build_query_for_int_value_gte_operation() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let result = RepositoryPostgres::build_query_for(
        query,
        "read_count".to_string(),
        FilterOperation::Gte,
        10,
    );
    let query_str = result.unwrap().into_sql();
    assert_eq!(query_str, " AND read_count >= $1");
}

#[test]
fn build_query_for_int_value_lte_operation() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let result = RepositoryPostgres::build_query_for(
        query,
        "read_count".to_string(),
        FilterOperation::Lte,
        10,
    );
    let query_str = result.unwrap().into_sql();
    assert_eq!(query_str, " AND read_count <= $1");
}
// endregion:   ======= int value =======

// region:      ======= bool value =======

#[test]
fn build_query_for_bool_value_unsupport_operation() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let result = RepositoryPostgres::build_query_for(
        query,
        "is_published".to_string(),
        FilterOperation::NotContains,
        true,
    );
    assert!(result.is_err());
    let error = result.err().unwrap();
    let expected_error = Error::FilterOperationNotSupport {
        value_type: "Bool value".to_string(),
        support_operation: vec![FilterOperation::Eq],
        actual_operation: FilterOperation::NotContains,
    };
    assert_eq!(error.to_string(), expected_error.to_string());
}

#[test]
fn build_query_for_bool_value_eq_operation() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let result = RepositoryPostgres::build_query_for(
        query,
        "is_published".to_string(),
        FilterOperation::Eq,
        true,
    );
    let query_str = result.unwrap().into_sql();
    assert_eq!(query_str, " AND is_published = $1");
}

// endregion:   ======= bool value =======

// region:      ======= string array value =======
fn create_vec_of_string() -> Vec<String> {
    vec![
        "Fiction".to_string(),
        "Science".to_string(),
        "Comedy".to_string(),
        "Fantasy".to_string(),
        "Slice of Life".to_string(),
    ]
}

#[test]
fn build_query_for_string_array_value_unsupport_operation() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let result = RepositoryPostgres::build_query_for(
        query,
        "tags".to_string(),
        FilterOperation::NotContains,
        create_vec_of_string(),
    );
    assert!(result.is_err());
    let error = result.err().unwrap();
    let expected_error = Error::FilterOperationNotSupport {
        value_type: "String array value".to_string(),
        support_operation: vec![
            FilterOperation::In,
            FilterOperation::NotIn,
        ],
        actual_operation: FilterOperation::NotContains,
    };
    assert_eq!(error.to_string(), expected_error.to_string());
}

#[test]
fn build_query_for_string_array_value_in_operation() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let result = RepositoryPostgres::build_query_for(
        query,
        "tags".to_string(),
        FilterOperation::In,
        create_vec_of_string(),
    );
    let query_str = result.unwrap().into_sql();
    assert_eq!(query_str, " AND tags IN ($1, $2, $3, $4, $5) ");
}

#[test]
fn build_query_for_string_array_value_not_in_operation() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let result = RepositoryPostgres::build_query_for(
        query,
        "tags".to_string(),
        FilterOperation::NotIn,
        create_vec_of_string(),
    );
    let query_str = result.unwrap().into_sql();
    assert_eq!(query_str, " AND tags NOT IN ($1, $2, $3, $4, $5) ");
}

// endregion:   ======= string array value =======

// region:      ======= date time value =======
#[test]
fn build_query_for_date_time_value_gte_operation() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let result = RepositoryPostgres::build_query_for(
        query,
        "created_at".to_string(),
        FilterOperation::Gte,
        time::try_parse_utc("2024-01-01T00:00:00Z").unwrap(),
    );
    let query_str = result.unwrap().into_sql();
    assert_eq!(query_str, " AND created_at >= $1");
}

#[test]
fn build_query_for_date_time_value_lte_operation() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let result = RepositoryPostgres::build_query_for(
        query,
        "created_at".to_string(),
        FilterOperation::Lte,
        time::try_parse_utc("2024-01-01T00:00:00Z").unwrap(),
    );
    let query_str = result.unwrap().into_sql();
    assert_eq!(query_str, " AND created_at <= $1");
}

#[test]
fn build_query_for_date_time_value_operation_not_support() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let result = RepositoryPostgres::build_query_for(
        query,
        "created_at".to_string(),
        FilterOperation::Contains,
        time::try_parse_utc("2024-01-01T00:00:00Z").unwrap(),
    );
    assert!(result.is_err());
    let error = result.err().unwrap();
    let expected_error = Error::FilterOperationNotSupport {
        value_type: "Date Time value".to_string(),
        actual_operation: FilterOperation::Contains,
        support_operation: vec![
            FilterOperation::Gte,
            FilterOperation::Lte,
        ],
    };
    assert_eq!(error.to_string(), expected_error.to_string());
}

// endregion:   ======= date time value =======

// region:      ======= pagination =======
#[test]
fn build_query_for_pagination_ok() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let pagination_input: PaginationInput = PaginationInput {
        page: 3,
        per_page: 50,
    };
    let result =
        RepositoryPostgres::build_query_for_pagination(query, pagination_input);
    let query_str = result.unwrap().into_sql();
    assert_eq!(query_str, " LIMIT 50 OFFSET 100 ");
}
#[test]
fn build_query_for_pagination_per_page_larger_than_max() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let pagination_input: PaginationInput = PaginationInput {
        page: 3,
        per_page: 201,
    };
    let result =
        RepositoryPostgres::build_query_for_pagination(query, pagination_input);
    assert!(result.is_err());
    let error = result.err().unwrap();
    let expected_error = Error::PaginationValueOutOfRange {
        value_name: "per_page".to_string(),
        actual_value: 201,
        min: 1,
        max: 200,
    };
    assert_eq!(error.to_string(), expected_error.to_string());
}

#[test]
fn build_query_for_pagination_per_page_lower_than_min() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let pagination_input: PaginationInput = PaginationInput {
        page: 3,
        per_page: 0,
    };
    let result =
        RepositoryPostgres::build_query_for_pagination(query, pagination_input);
    assert!(result.is_err());
    let error = result.err().unwrap();
    let expected_error = Error::PaginationValueOutOfRange {
        value_name: "per_page".to_string(),
        actual_value: 0,
        min: 1,
        max: 200,
    };
    assert_eq!(error.to_string(), expected_error.to_string());
}

#[test]
fn build_query_for_pagination_page_lower_than_min() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let pagination_input: PaginationInput = PaginationInput {
        page: 0,
        per_page: 10,
    };
    let result =
        RepositoryPostgres::build_query_for_pagination(query, pagination_input);
    assert!(result.is_err());
    let error = result.err().unwrap();
    let expected_error = Error::PaginationValueOutOfRange {
        value_name: "page".to_string(),
        actual_value: 0,
        min: 1,
        max: i32::MAX,
    };
    assert_eq!(error.to_string(), expected_error.to_string());
}

// endregion:      ======= pagination =======

// region:      ======= sort =======
struct TestSortInput {
    field: String,
    order: SortOrder,
}
impl SortInput for TestSortInput {
    fn field_name_as_string(&self) -> String {
        self.field.clone()
    }

    fn order(&self) -> SortOrder {
        self.order.clone()
    }
}

#[test]
fn build_query_for_sort_ok() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");

    let mut sort_inputs: Vec<TestSortInput> = Vec::new();
    let sort_by_id_asc: TestSortInput = TestSortInput {
        field: "id".to_string(),
        order: SortOrder::Asc,
    };
    sort_inputs.push(sort_by_id_asc);
    let sort_by_created_at_desc: TestSortInput = TestSortInput {
        field: "created_at".to_string(),
        order: SortOrder::Desc,
    };
    sort_inputs.push(sort_by_created_at_desc);

    let result = RepositoryPostgres::build_query_for_sort(query, sort_inputs);
    let query_str = result.unwrap().into_sql();
    assert_eq!(query_str, " ORDER BY id asc, created_at desc");
}

#[test]
fn build_query_for_sort_input_empty() {
    let query: sqlx::QueryBuilder<sqlx::Postgres> = sqlx::QueryBuilder::new("");
    let sort_inputs: Vec<TestSortInput> = Vec::new();

    let result = RepositoryPostgres::build_query_for_sort(query, sort_inputs);
    assert!(result.is_err());
    let expected_error = Error::SortInputEmpty;
    assert_eq!(
        result.err().unwrap().to_string(),
        expected_error.to_string()
    );
}
// endregion:      ======= sort =======

// region:      ======= try_inputs_length_in_range =======
#[test]
fn try_inputs_length_in_range_ok() {
    let result = RepositoryPostgres::try_inputs_length_in_range(100);
    assert!(result.is_ok());
}

#[test]
fn try_inputs_length_in_range_error() {
    let result = RepositoryPostgres::try_inputs_length_in_range(101);
    assert!(result.is_err());
    let error = result.err().unwrap();
    let expected_error = Error::FilterInputsTooLong {
        actual_len: 101,
        max_len: 100,
    };
    assert_eq!(error.to_string(), expected_error.to_string());
}
// endregion:      ======= try_inputs_length_in_range =======
