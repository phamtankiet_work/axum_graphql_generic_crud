use lib_crud::filter::FilterOperation;
use serde::{Deserialize, Serialize};

pub type Result<T> = core::result::Result<T, Error>;

#[derive(Debug, Serialize, Deserialize)]
pub enum Error {
    FilterValueOnlyAllowedOneField,
    FilterOperationNotSupport {
        value_type: String,
        support_operation: Vec<FilterOperation>,
        actual_operation: FilterOperation,
    },
    FilterInputsTooLong {
        actual_len: i32,
        max_len: i32,
    },
    PaginationValueOutOfRange {
        value_name: String,
        actual_value: i32,
        min: i32,
        max: i32,
    },
    SortInputEmpty,
}

// region:      ======= Error Boilerplate =======
impl core::fmt::Display for Error {
    fn fmt(
        &self,
        fmt: &mut core::fmt::Formatter,
    ) -> core::result::Result<(), core::fmt::Error> {
        write!(fmt, "{self:?}")
    }
}

impl std::error::Error for Error {}

// endregion:      ======= Error Boilerplate  =======
