mod error;
pub mod repository_postgres;

pub use error::{Error, Result};

#[cfg(test)]
mod tests;
