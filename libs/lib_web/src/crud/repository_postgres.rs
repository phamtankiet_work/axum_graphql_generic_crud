use super::{Error, Result};
use lib_crud::{
    filter::{FilterInput, FilterOperation, FilterValue},
    PaginationInput, SortInput,
};
use lib_utils::{time, uuid::Uuid};
use sqlx::QueryBuilder;

type QueryBuilderPostGres<'a> = QueryBuilder<'a, sqlx::Postgres>;

pub struct RepositoryPostgres {}

impl RepositoryPostgres {
    fn build_query_for_each_filter(
        query: QueryBuilderPostGres,
        field_name: String,
        operation: FilterOperation,
        filter_value: FilterValue,
    ) -> Result<QueryBuilderPostGres> {
        match (
            filter_value.uuid_value,
            filter_value.uuid_array_value,
            filter_value.string_value,
            filter_value.int_value,
            filter_value.bool_value,
            filter_value.string_array_value,
            filter_value.date_time_value,
        ) {
            (Some(value), None, None, None, None, None, None) => {
                Self::build_query_for(query, field_name, operation, value)
            }
            (None, Some(value), None, None, None, None, None) => {
                Self::build_query_for(query, field_name, operation, value)
            }
            (None, None, Some(value), None, None, None, None) => {
                Self::build_query_for(query, field_name, operation, value)
            }
            (None, None, None, Some(value), None, None, None) => {
                Self::build_query_for(query, field_name, operation, value)
            }
            (None, None, None, None, Some(value), None, None) => {
                Self::build_query_for(query, field_name, operation, value)
            }
            (None, None, None, None, None, Some(value), None) => {
                Self::build_query_for(query, field_name, operation, value)
            }
            (None, None, None, None, None, None, Some(value)) => {
                Self::build_query_for(query, field_name, operation, value)
            }
            // return error on 2^7-7  (121) other cases
            (_, _, _, _, _, _, _) => Err(Error::FilterValueOnlyAllowedOneField),
        }
    }

    pub fn try_inputs_length_in_range(actual_len: usize) -> Result<()> {
        let max_length = 100;
        if actual_len > max_length {
            Err(Error::FilterInputsTooLong {
                actual_len: actual_len as i32,
                max_len: max_length as i32,
            })
        } else {
            Ok(())
        }
    }
    pub fn build_query_for_filter(
        query: QueryBuilderPostGres,
        filter_inputs: Vec<impl FilterInput>,
    ) -> Result<QueryBuilderPostGres> {
        Self::try_inputs_length_in_range(filter_inputs.len())?;
        let modified_query = filter_inputs.into_iter().try_fold(
            query,
            |modified_query, each_filter_input| {
                Self::build_query_for_each_filter(
                    modified_query,
                    each_filter_input.field_name_as_string(),
                    each_filter_input.operation(),
                    each_filter_input.value(),
                )
            },
        )?;
        Ok(modified_query)
    }

    pub fn build_query_for_pagination(
        mut query: QueryBuilderPostGres,
        pagination_input: PaginationInput,
    ) -> Result<QueryBuilderPostGres> {
        let limit: i32 = pagination_input.per_page;
        if !(1..=200).contains(&limit) {
            return Err(Error::PaginationValueOutOfRange {
                value_name: "per_page".to_string(),
                actual_value: limit,
                min: 1,
                max: 200,
            });
        }
        if pagination_input.page < 1 {
            return Err(Error::PaginationValueOutOfRange {
                value_name: "page".to_string(),
                actual_value: pagination_input.page,
                min: 1,
                max: i32::MAX,
            });
        }
        let offset: i32 = (pagination_input.page - 1) * limit;

        query.push(" LIMIT ");
        query.push(limit);

        query.push(" OFFSET ");
        query.push(offset);
        query.push(" ");
        Ok(query)
    }

    pub fn build_query_for_sort(
        mut query: QueryBuilderPostGres,
        sort_inputs: Vec<impl SortInput>,
    ) -> Result<QueryBuilderPostGres> {
        // ORDER BY id ASC, second DESC, third ASC
        // This sorts everything by field name with order first,
        // and then by next array value field name and order
        // whenever the field value of field name before for two or more rows are equal.
        if sort_inputs.is_empty() {
            return Err(Error::SortInputEmpty);
        }
        query.push(" ORDER BY ");
        let mut separated = query.separated(", ");
        sort_inputs
            .into_iter()
            .for_each(|each| {
                let sql = format!(
                    "{} {}",
                    each.field_name_as_string(),
                    each.order().to_str()
                );
                separated.push(sql);
            });
        separated.push_unseparated("");

        Ok(query)
    }
}

#[derive(Debug, sqlx::FromRow)]
pub struct QueryWithTotal<T> {
    #[sqlx(flatten)]
    pub item: T,
    pub total_count: i64,
}

// region:      ======= Filter Operation Boilerplate =======

trait OperationToSql {
    fn to_sql(&self) -> String;
}

impl OperationToSql for FilterOperation {
    fn to_sql(&self) -> String {
        match self {
            FilterOperation::Eq => String::from("="),
            FilterOperation::Neq => String::from("!="),
            FilterOperation::Gt => String::from(">"),
            FilterOperation::Gte => String::from(">="),
            FilterOperation::Lt => String::from("<"),
            FilterOperation::Lte => String::from("<="),
            FilterOperation::In => String::from("IN"),
            FilterOperation::NotIn => String::from("NOT IN"),
            FilterOperation::Contains => String::from("ILIKE"),
            FilterOperation::NotContains => String::from("NOT ILIKE"),
        }
    }
}
fn query_push_operation(
    mut query: QueryBuilderPostGres,
    field_name: String,
    operation: FilterOperation,
) -> QueryBuilderPostGres {
    query.push(" AND ");
    query.push(field_name);
    query.push(" ");
    query.push(operation.to_sql());
    query.push(" ");
    query
}

pub trait BuildQueryForType<T> {
    fn build_query_for(
        query: QueryBuilderPostGres,
        field_name: String,
        operation: FilterOperation,
        value: T,
    ) -> Result<QueryBuilderPostGres>;
}

impl BuildQueryForType<Uuid> for RepositoryPostgres {
    fn build_query_for(
        mut query: QueryBuilderPostGres,
        field_name: String,
        operation: FilterOperation,
        value: Uuid,
    ) -> Result<QueryBuilderPostGres> {
        match operation {
            FilterOperation::Eq => {
                query = query_push_operation(query, field_name, operation);
                query.push_bind(value);
                Ok(query)
            }
            _ => Err(Error::FilterOperationNotSupport {
                value_type: "Uuid value".to_string(),
                support_operation: vec![FilterOperation::Eq],
                actual_operation: operation,
            }),
        }
    }
}

impl BuildQueryForType<Vec<Uuid>> for RepositoryPostgres {
    fn build_query_for(
        mut query: QueryBuilderPostGres,
        field_name: String,
        operation: FilterOperation,
        values: Vec<Uuid>,
    ) -> Result<QueryBuilderPostGres> {
        match operation {
            FilterOperation::In | FilterOperation::NotIn => {
                query = query_push_operation(query, field_name, operation);
                query.push("(");
                let mut separated = query.separated(", ");
                values.into_iter().for_each(|each| {
                    separated.push_bind(each);
                });
                separated.push_unseparated(") ");
                Ok(query)
            }
            _ => Err(Error::FilterOperationNotSupport {
                value_type: "Uuid array value".to_string(),
                support_operation: vec![
                    FilterOperation::In,
                    FilterOperation::NotIn,
                ],
                actual_operation: operation,
            }),
        }
    }
}

impl BuildQueryForType<String> for RepositoryPostgres {
    fn build_query_for(
        mut query: QueryBuilderPostGres,
        field_name: String,
        operation: FilterOperation,
        value: String,
    ) -> Result<QueryBuilderPostGres> {
        match operation {
            FilterOperation::Eq | FilterOperation::Neq => {
                query = query_push_operation(query, field_name, operation);
                query.push_bind(value);
                Ok(query)
            }
            FilterOperation::Contains | FilterOperation::NotContains => {
                query = query_push_operation(query, field_name, operation);
                query.push("concat('%', ");
                query.push_bind(value);
                query.push(", '%')");
                Ok(query)
            }
            _ => Err(Error::FilterOperationNotSupport {
                value_type: "String value".to_string(),
                support_operation: vec![
                    FilterOperation::Eq,
                    FilterOperation::Neq,
                    FilterOperation::Contains,
                    FilterOperation::NotContains,
                ],
                actual_operation: operation,
            }),
        }
    }
}

impl BuildQueryForType<i32> for RepositoryPostgres {
    fn build_query_for(
        mut query: QueryBuilderPostGres,
        field_name: String,
        operation: FilterOperation,
        value: i32,
    ) -> Result<QueryBuilderPostGres> {
        match operation {
            FilterOperation::Eq
            | FilterOperation::Neq
            | FilterOperation::Gt
            | FilterOperation::Lt
            | FilterOperation::Gte
            | FilterOperation::Lte => {
                query = query_push_operation(query, field_name, operation);
                query.push_bind(value);
                Ok(query)
            }
            _ => Err(Error::FilterOperationNotSupport {
                value_type: "Integer value".to_string(),
                support_operation: vec![
                    FilterOperation::Eq,
                    FilterOperation::Neq,
                    FilterOperation::Gt,
                    FilterOperation::Lt,
                    FilterOperation::Gte,
                    FilterOperation::Lte,
                ],
                actual_operation: operation,
            }),
        }
    }
}

impl BuildQueryForType<bool> for RepositoryPostgres {
    fn build_query_for(
        mut query: QueryBuilderPostGres,
        field_name: String,
        operation: FilterOperation,
        value: bool,
    ) -> Result<QueryBuilderPostGres> {
        match operation {
            FilterOperation::Eq => {
                query = query_push_operation(query, field_name, operation);
                query.push_bind(value);
                Ok(query)
            }
            _ => Err(Error::FilterOperationNotSupport {
                value_type: "Bool value".to_string(),
                support_operation: vec![FilterOperation::Eq],
                actual_operation: operation,
            }),
        }
    }
}

impl BuildQueryForType<Vec<String>> for RepositoryPostgres {
    fn build_query_for(
        mut query: QueryBuilderPostGres,
        field_name: String,
        operation: FilterOperation,
        values: Vec<String>,
    ) -> Result<QueryBuilderPostGres> {
        match operation {
            FilterOperation::In | FilterOperation::NotIn => {
                query = query_push_operation(query, field_name, operation);
                query.push("(");
                let mut separated = query.separated(", ");
                values.into_iter().for_each(|each| {
                    separated.push_bind(each);
                });
                separated.push_unseparated(") ");
                Ok(query)
            }
            _ => Err(Error::FilterOperationNotSupport {
                value_type: "String array value".to_string(),
                support_operation: vec![
                    FilterOperation::In,
                    FilterOperation::NotIn,
                ],
                actual_operation: operation,
            }),
        }
    }
}

impl BuildQueryForType<time::OffsetDateTime> for RepositoryPostgres {
    fn build_query_for(
        mut query: QueryBuilderPostGres,
        field_name: String,
        operation: FilterOperation,
        value: time::OffsetDateTime,
    ) -> Result<QueryBuilderPostGres> {
        match operation {
            FilterOperation::Gte | FilterOperation::Lte => {
                query = query_push_operation(query, field_name, operation);
                query.push_bind(value);
                Ok(query)
            }
            _ => Err(Error::FilterOperationNotSupport {
                value_type: "Date Time value".to_string(),
                support_operation: vec![
                    FilterOperation::Gte,
                    FilterOperation::Lte,
                ],
                actual_operation: operation,
            }),
        }
    }
}
// endregion:      ======= Filter Operation Boilerplate =======
