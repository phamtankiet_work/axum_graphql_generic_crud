use lib_utils::env::{try_get_env, try_get_env_parse};

mod error;
// pub mod mongodb;
mod postgres;

pub use error::{Error, Result};
pub use postgres::PostgresConfig;

pub trait FetchConfig {
    fn fetch_config() -> Result<Self>
    where
        Self: Sized;
}
