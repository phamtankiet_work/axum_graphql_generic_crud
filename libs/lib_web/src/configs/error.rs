use lib_utils::env::Error as EnvError;

pub type Result<T> = core::result::Result<T, Error>;

#[derive(Debug)]
pub enum Error {
    EnvError(EnvError),
    ValueOutOfRange {
        min: u64,
        max: u64,
        actual: u64,
        name: &'static str,
    },
}

// region:      ======= Error Boilerplate =======
impl core::fmt::Display for Error {
    fn fmt(
        &self,
        fmt: &mut core::fmt::Formatter,
    ) -> core::result::Result<(), core::fmt::Error> {
        write!(fmt, "{self:?}")
    }
}

impl std::error::Error for Error {}

impl From<EnvError> for Error {
    fn from(error: EnvError) -> Self {
        Self::EnvError(error)
    }
}
// region:      ======= Error Boilerplate  =======
