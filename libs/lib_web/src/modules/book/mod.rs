pub mod error;

pub mod entity;
// pub mod repository;
pub mod repository_postgres;
pub mod service;

pub use error::{Error, Result};

#[cfg(test)]
mod tests;
