use super::entity::{Book, FindAllBookInput, FindAllBookResult};
use crate::crud::repository_postgres::{QueryWithTotal, RepositoryPostgres};
use crate::database::ConnectionManager;
use lib_crud::{PaginationInput, PaginationResult};
use lib_utils::time::now_utc;
use lib_utils::uuid::Uuid;
use sqlx::{PgPool, QueryBuilder};

use super::{Error, Result};

pub struct BookRepositoryPostgres {}

impl BookRepositoryPostgres {
    pub async fn find_all(
        connection_manager: &impl ConnectionManager<PgPool>,
        find_all_input: FindAllBookInput,
    ) -> Result<FindAllBookResult> {
        let pagination: PaginationInput = find_all_input
            .pagination
            .unwrap_or_default();
        let mut query: QueryBuilder<sqlx::Postgres> = QueryBuilder::new("");

        // select
        query.push("SELECT id, title, created_at, updated_at");
        query.push(", count(1) over() as total_count"); // support for pagination
        query.push(" FROM books");

        // join, on
        // where
        query.push(" WHERE 1=1 "); // add 1=1 allow to use AND command without check if operation is first or not
        if let Some(filter_inputs) = find_all_input.filter {
            query = RepositoryPostgres::build_query_for_filter(
                query,
                filter_inputs,
            )?;
        }
        // groupby
        // having
        // order by
        if let Some(sort_inputs) = find_all_input.sort {
            query =
                RepositoryPostgres::build_query_for_sort(query, sort_inputs)
                    .unwrap();
        } else {
            query.push(" ORDER BY id ASC ");
        }

        // limit, offset
        query = RepositoryPostgres::build_query_for_pagination(
            query,
            find_all_input
                .pagination
                .unwrap_or_default(),
        )?;
        println!("sql: {}", query.sql());

        let query_result = query
            .build_query_as::<QueryWithTotal<Book>>()
            .fetch_all(&connection_manager.get_connection())
            .await;

        match query_result {
            Ok(entities) => {
                if entities.is_empty() {
                    return Ok(FindAllBookResult {
                        items: vec![],
                        pagination: PaginationResult::new(
                            pagination.page,
                            pagination.per_page,
                            0,
                        ),
                    });
                }
                let total_result: i64 = entities.first().unwrap().total_count;
                let books = entities
                    .into_iter()
                    .map(|entity| entity.item)
                    .collect();
                Ok(FindAllBookResult {
                    items: books,
                    pagination: PaginationResult::new(
                        pagination.page,
                        pagination.per_page,
                        total_result as i32,
                    ),
                })
            }
            Err(error) => {
                println!("{:?}", error);
                unimplemented!("Error handling for find_all");
            }
        }
    }

    pub async fn find_one_by_id(
        connection_manager: &impl ConnectionManager<PgPool>,
        id: Uuid,
    ) -> Result<Book> {
        sqlx::query_as::<_, Book>("SELECT * FROM books WHERE id = $1")
            .bind(id)
            .fetch_one(&connection_manager.get_connection())
            .await
            .map_err(|error: sqlx::Error| match error {
                sqlx::Error::RowNotFound => Error::BookNotFound(id),
                _ => Error::DatabaseError(error.to_string()),
            })
    }

    pub async fn create_one(
        connection_manager: &impl ConnectionManager<PgPool>,
        entity: Book,
    ) -> Result<Book> {
        let entity = Book::fill_data_to_save(entity);

        sqlx::query_as::<_, Book>(
            r#"
            INSERT INTO books (id, title, created_at, updated_at)
            VALUES ($1, $2, $3, $4)
            RETURNING *
            "#,
        )
        .bind(entity.id)
        .bind(entity.title)
        .bind(entity.created_at)
        .bind(entity.updated_at)
        .fetch_one(&connection_manager.get_connection())
        .await
        .map_err(|error: sqlx::Error| Error::DatabaseError(error.to_string()))
    }

    pub async fn update_one_by_id(
        connection_manager: &impl ConnectionManager<PgPool>,
        id: Uuid,
        update_input: Book,
    ) -> Result<Book> {
        sqlx::query_as::<_, Book>(
            r#"
            UPDATE books
            SET
                title = $2,
                updated_at = $3
            WHERE id = $1
            RETURNING *
            "#,
        )
        .bind(id)
        .bind(update_input.title)
        .bind(now_utc())
        .fetch_one(&connection_manager.get_connection())
        .await
        .map_err(|error: sqlx::Error| Error::DatabaseError(error.to_string()))
    }

    pub async fn delete_one_by_id(
        connection_manager: &impl ConnectionManager<PgPool>,
        id: Uuid,
    ) -> Result<Book> {
        sqlx::query_as::<_, Book>(
            r#"
            DELETE FROM books
            WHERE id = $1
            RETURNING *
            "#,
        )
        .bind(id)
        .fetch_one(&connection_manager.get_connection())
        .await
        .map_err(|error: sqlx::Error| match error {
            sqlx::Error::RowNotFound => Error::BookNotFound(id),
            _ => Error::DatabaseError(error.to_string()),
        })
    }
}
