use super::entity::*;
use super::{Error, Result};
use crate::database::ConnectionManager;
use lib_utils::uuid::Uuid;

use super::repository_postgres::BookRepositoryPostgres as BookRepository;
use sqlx::PgPool;
type PoolType = PgPool;
pub struct BookService {}

impl BookService {
    pub async fn find_all(
        connection_manager: &impl ConnectionManager<PoolType>,
        find_all_input: FindAllBookInput,
    ) -> Result<FindAllBookResult> {
        BookRepository::find_all(connection_manager, find_all_input).await
    }

    pub async fn find_one_by_id(
        connection_manager: &impl ConnectionManager<PoolType>,
        id: Uuid,
    ) -> Result<Book> {
        BookRepository::find_one_by_id(connection_manager, id).await
    }

    pub async fn create_one(
        connection_manager: &impl ConnectionManager<PoolType>,
        create_input: CreateBookInput,
    ) -> Result<Book> {
        Self::check_valid_title_input(create_input.title.clone())?;
        let entity_to_create = Book {
            id: None,
            title: Some(create_input.title),
            created_at: None,
            updated_at: None,
        };
        BookRepository::create_one(connection_manager, entity_to_create).await
    }

    pub async fn update_one_by_id(
        connection_manager: &impl ConnectionManager<PoolType>,
        id: Uuid,
        update_input: UpdateBookInput,
    ) -> Result<Book> {
        Self::check_valid_title_input(update_input.title.clone())?;
        let entity_to_update = Book {
            id: None,
            title: Some(update_input.title),
            created_at: None,
            updated_at: None,
        };
        BookRepository::update_one_by_id(
            connection_manager,
            id,
            entity_to_update,
        )
        .await
    }

    pub async fn delete_one_by_id(
        connection_manager: &impl ConnectionManager<PoolType>,
        id: Uuid,
    ) -> Result<Book> {
        BookRepository::delete_one_by_id(connection_manager, id).await
    }
}

impl BookService {
    fn check_valid_title_input(title: String) -> Result<()> {
        let max_len: i32 = 255;
        let actual_len = title.len() as i32;
        if actual_len > max_len {
            return Err(Error::BookTitleTooLong {
                actual_len,
                max_len,
            });
        }
        Ok(())
    }
}
