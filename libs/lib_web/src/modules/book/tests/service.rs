use lib_crud::{FilterOperation, FilterValue, PaginationInput, SortOrder};
use lib_utils::{
    time::{self},
    uuid,
};
use sqlx::PgPool;

use super::*;

// region:      ======= create =======
#[sqlx::test]
async fn create_one_success(pool: PgPool) -> sqlx::Result<()> {
    let connection_manager =
        common::prepare_postgres_manager_with_pool(pool).await;
    let create_book_input: CreateBookInput = CreateBookInput {
        title: "book_name".to_string(),
    };
    let result =
        BookService::create_one(&connection_manager, create_book_input)
            .await
            .unwrap();
    assert_eq!(result.title.unwrap(), "book_name".to_string());
    Ok(())
}

#[sqlx::test]
async fn create_one_title_too_long(pool: PgPool) -> sqlx::Result<()> {
    let connection_manager =
        common::prepare_postgres_manager_with_pool(pool).await;
    let create_book_input: CreateBookInput = CreateBookInput {
        title: (0..256)
            .map(|_| "x")
            .collect::<String>(),
    };
    let result =
        BookService::create_one(&connection_manager, create_book_input).await;
    assert!(result.is_err());
    let error = result.err().unwrap();
    let expected_error = Error::BookTitleTooLong {
        actual_len: 256,
        max_len: 255,
    };
    assert_eq!(error.to_string(), expected_error.to_string());
    Ok(())
}

// endregion:   ======= create =======

// region:      ======= find_one_by_id =======
#[sqlx::test(fixtures("books_10"))]
async fn find_one_by_id_success(pool: PgPool) -> sqlx::Result<()> {
    // ('019146a0-ed01-7dc0-9e97-02f952f4300a'::uuid,'Passenger','2016-01-01 07:00:00.000','2016-01-01 07:00:00.000'),
    let connection_manager =
        common::prepare_postgres_manager_with_pool(pool).await;
    let result = BookService::find_one_by_id(
        &connection_manager,
        uuid::try_parse("019146a0-ed01-7dc0-9e97-02f952f4300a").unwrap(),
    )
    .await
    .unwrap();
    assert_eq!(result.title.unwrap(), "Passenger".to_string());
    Ok(())
}

#[sqlx::test]
async fn find_one_by_id_not_found(pool: PgPool) -> sqlx::Result<()> {
    // ('019146a0-ed01-7dc0-9e97-02f952f4300a'::uuid,'Passenger','2016-01-01 07:00:00.000','2016-01-01 07:00:00.000'),
    let id = uuid::try_parse("019146a0-ed01-7dc0-9e97-02f952f4300a").unwrap();
    let connection_manager =
        common::prepare_postgres_manager_with_pool(pool).await;
    let result = BookService::find_one_by_id(&connection_manager, id).await;
    assert!(result.is_err());
    let error = result.err().unwrap();
    let expected_error = Error::BookNotFound(id);
    assert_eq!(error.to_string(), expected_error.to_string());
    Ok(())
}

// endregion:   ======= find_one_by_id =======

// region:      ======= update_one_by_id =======
#[sqlx::test(fixtures("books_10"))]
async fn update_one_by_id_success(pool: PgPool) -> sqlx::Result<()> {
    // ('019146a0-ed01-7dc0-9e97-02f952f4300a'::uuid,'Passenger','2016-01-01 07:00:00.000','2016-01-01 07:00:00.000'),
    let connection_manager =
        common::prepare_postgres_manager_with_pool(pool).await;

    let target_book_uuid =
        uuid::try_parse("019146a0-ed01-7dc0-9e97-02f952f4300a").unwrap();
    let new_book_title = "new_title".to_string();
    let origin_book =
        BookService::find_one_by_id(&connection_manager, target_book_uuid)
            .await
            .unwrap();

    let updated_book = BookService::update_one_by_id(
        &connection_manager,
        target_book_uuid,
        UpdateBookInput {
            title: new_book_title.clone().to_string(),
        },
    )
    .await
    .unwrap();
    assert_ne!(
        origin_book.title.unwrap(),
        updated_book.title.clone().unwrap()
    );
    assert_eq!(updated_book.title.unwrap(), new_book_title.to_string());
    Ok(())
}

#[sqlx::test(fixtures("books_10"))]
async fn update_one_by_id_title_too_long(pool: PgPool) -> sqlx::Result<()> {
    // ('019146a0-ed01-7dc0-9e97-02f952f4300a'::uuid,'Passenger','2016-01-01 07:00:00.000','2016-01-01 07:00:00.000'),
    let connection_manager =
        common::prepare_postgres_manager_with_pool(pool).await;

    let target_book_uuid =
        uuid::try_parse("019146a0-ed01-7dc0-9e97-02f952f4300a").unwrap();
    let new_book_title = (0..256)
        .map(|_| "x")
        .collect::<String>();
    let updated_book = BookService::update_one_by_id(
        &connection_manager,
        target_book_uuid,
        UpdateBookInput {
            title: new_book_title.clone().to_string(),
        },
    )
    .await;
    assert!(updated_book.is_err());
    let error = updated_book.err().unwrap();
    let expected_error = Error::BookTitleTooLong {
        actual_len: 256,
        max_len: 255,
    };
    assert_eq!(error.to_string(), expected_error.to_string());
    Ok(())
}

// endregion:   ======= update_one_by_id =======

// region:      ======= delete_one_by_id =======
#[sqlx::test(fixtures("books_10"))]
async fn delete_one_by_id_success(pool: PgPool) -> sqlx::Result<()> {
    // ('019146a0-ed01-7dc0-9e97-02f952f4300a'::uuid,'Passenger','2016-01-01 07:00:00.000','2016-01-01 07:00:00.000'),
    let connection_manager =
        common::prepare_postgres_manager_with_pool(pool).await;
    let target_book_uuid =
        uuid::try_parse("019146a0-ed01-7dc0-9e97-02f952f4300a").unwrap();
    let result =
        BookService::delete_one_by_id(&connection_manager, target_book_uuid)
            .await;
    assert_eq!(target_book_uuid, result.unwrap().id.unwrap());
    Ok(())
}

#[sqlx::test]
async fn delete_one_by_id_not_found(pool: PgPool) -> sqlx::Result<()> {
    // ('019146a0-ed01-7dc0-9e97-02f952f4300a'::uuid,'Passenger','2016-01-01 07:00:00.000','2016-01-01 07:00:00.000'),
    let connection_manager =
        common::prepare_postgres_manager_with_pool(pool).await;
    let target_book_uuid =
        uuid::try_parse("019146a0-ed01-7dc0-9e97-02f952f4300a").unwrap();
    let result =
        BookService::delete_one_by_id(&connection_manager, target_book_uuid)
            .await;
    assert!(result.is_err());
    let error = result.err().unwrap();
    let expected_error = Error::BookNotFound(target_book_uuid);
    assert_eq!(error.to_string(), expected_error.to_string());
    Ok(())
}

// endregion:   ======= delete_one_by_id =======

// region:      ======= find_all =======
#[sqlx::test(fixtures("books"))]
async fn find_all_default(pool: PgPool) -> sqlx::Result<()> {
    let connection_manager =
        common::prepare_postgres_manager_with_pool(pool).await;
    let find_all_input_default: FindAllBookInput = FindAllBookInput {
        ..Default::default()
    };
    let result =
        BookService::find_all(&connection_manager, find_all_input_default)
            .await
            .unwrap();
    assert_eq!(10, result.items.len());
    Ok(())
}

// region:          ======= pagination =======
#[sqlx::test(fixtures("books"))]
async fn find_all_limit(pool: PgPool) -> sqlx::Result<()> {
    let connection_manager =
        common::prepare_postgres_manager_with_pool(pool).await;
    let find_all_input_limit_20: FindAllBookInput = FindAllBookInput {
        text_search: None,
        filter: None,
        pagination: Some(PaginationInput {
            page: 1,
            per_page: 20,
        }),
        sort: None,
    };

    let result =
        BookService::find_all(&connection_manager, find_all_input_limit_20)
            .await
            .unwrap();
    assert_eq!(20, result.items.len());
    Ok(())
}
// endregion:      ======= pagination =======

// region:          ======= order =======
#[sqlx::test(fixtures("books_10"))]
async fn find_all_order_by(pool: PgPool) -> sqlx::Result<()> {
    let connection_manager =
        common::prepare_postgres_manager_with_pool(pool).await;
    let find_all_input_order_by: FindAllBookInput = FindAllBookInput {
        text_search: None,
        filter: None,
        pagination: Some(PaginationInput {
            page: 1,
            per_page: 2,
        }),
        sort: Some(vec![BookSortInput {
            field_name: BookFieldName::CreatedAt,
            order: SortOrder::Desc,
        }]),
    };

    let books: Vec<Book> =
        BookService::find_all(&connection_manager, find_all_input_order_by)
            .await
            .unwrap()
            .items;
    let book_created_after = books[0].clone();
    let book_created_before = books[1].clone();
    let time_after = book_created_after.created_at.unwrap();
    let time_before = book_created_before.created_at.unwrap();
    assert!(time_before.lt(&time_after));
    Ok(())
}
// endregion:       ======= order =======

// region:          ======= filter =======

#[sqlx::test(fixtures("books_10"))]
async fn find_all_filter_empty(pool: PgPool) -> sqlx::Result<()> {
    let connection_manager =
        common::prepare_postgres_manager_with_pool(pool).await;
    let find_all_input_order_by: FindAllBookInput = FindAllBookInput {
        text_search: None,
        filter: Some(vec![BookFilterInput {
            field_name: BookFieldName::Title,
            operation: FilterOperation::Eq,
            value: FilterValue {
                string_value: Some("Test".to_string()),
                ..Default::default()
            },
        }]),
        pagination: Some(PaginationInput {
            page: 1,
            per_page: 10,
        }),
        sort: None,
    };

    let books: Vec<Book> =
        BookService::find_all(&connection_manager, find_all_input_order_by)
            .await
            .unwrap()
            .items;
    assert_eq!(0, books.len());
    Ok(())
}

#[sqlx::test(fixtures("books_10"))]
async fn find_all_filter_created_at_in_range(pool: PgPool) -> sqlx::Result<()> {
    let from_date = time::try_parse_utc("2019-01-01T00:00:00Z").unwrap();
    let to_date = time::try_parse_utc("2024-01-01T00:00:00Z").unwrap();
    let connection_manager =
        common::prepare_postgres_manager_with_pool(pool).await;
    let find_all_input: FindAllBookInput = FindAllBookInput {
        filter: Some(vec![
            BookFilterInput {
                field_name: BookFieldName::CreatedAt,
                operation: FilterOperation::Gte,
                value: FilterValue {
                    date_time_value: Some(from_date),
                    ..Default::default()
                },
            },
            BookFilterInput {
                field_name: BookFieldName::CreatedAt,
                operation: FilterOperation::Lte,
                value: FilterValue {
                    date_time_value: Some(to_date),
                    ..Default::default()
                },
            },
        ]),
        ..Default::default()
    };
    let results = BookService::find_all(&connection_manager, find_all_input)
        .await
        .unwrap()
        .items;
    assert_eq!(1, results.len());
    let book = results[0].clone();
    assert!(book.created_at.unwrap() >= from_date);
    assert!(book.created_at.unwrap() <= to_date);
    Ok(())
}

#[sqlx::test]
async fn find_all_filter_date_time_operation_not_support(pool: PgPool) {
    let date = time::try_parse_utc("2019-01-01T00:00:00Z").unwrap();
    let connection_manager =
        common::prepare_postgres_manager_with_pool(pool).await;
    let find_all_input: FindAllBookInput = FindAllBookInput {
        filter: Some(vec![BookFilterInput {
            field_name: BookFieldName::CreatedAt,
            operation: FilterOperation::Eq,
            value: FilterValue {
                date_time_value: Some(date),
                ..Default::default()
            },
        }]),
        ..Default::default()
    };
    let result =
        BookService::find_all(&connection_manager, find_all_input).await;
    assert!(result.is_err());
    let error = result.err().unwrap();
    let expected_error =
        Error::CrudError(crate::crud::Error::FilterOperationNotSupport {
            value_type: "Date Time value".to_string(),
            actual_operation: FilterOperation::Eq,
            support_operation: vec![
                FilterOperation::Gte,
                FilterOperation::Lte,
            ],
        });
    assert_eq!(error.to_string(), expected_error.to_string());
}
// endregion:       ======= filter =======

// endregion:   ======= find_all =======
