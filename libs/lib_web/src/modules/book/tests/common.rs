use sqlx::PgPool;

use crate::database::postgres::PostgresManager;

pub async fn prepare_postgres_manager_with_pool(
    pool: PgPool,
) -> PostgresManager {
    PostgresManager::create_with_pool_for_unit_test(pool).await
}
