use lib_crud::filter::{FilterOperation, FilterValue};
use lib_crud::{
    FilterInput, PaginationInput, PaginationResult, SortInput, SortOrder,
};
use lib_utils::strum;
use lib_utils::time::{now_utc, OffsetDateTime};
use lib_utils::uuid::{now_v7, Uuid};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize, sqlx::FromRow)]
pub struct Book {
    pub id: Option<Uuid>,
    pub title: Option<String>,
    pub created_at: Option<OffsetDateTime>,
    pub updated_at: Option<OffsetDateTime>,
}

impl Book {
    pub fn fill_data_to_save(self) -> Self {
        let mut data = self.clone();
        if data.id.is_none() {
            let id: Uuid = now_v7();
            data.id = Some(id);
        }
        if data.created_at.is_none() {
            data.created_at = Some(now_utc());
        }
        if data.updated_at.is_none() {
            data.updated_at = Some(now_utc());
        }
        data
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct CreateBookInput {
    pub title: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct UpdateBookInput {
    pub title: String,
}

#[derive(Debug, Serialize, Deserialize, strum::Display)]
#[strum(serialize_all = "snake_case")]
pub enum BookFieldName {
    Id,
    Title,
    CreatedAt,
    UpdatedAt,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct BookSortInput {
    pub field_name: BookFieldName,
    pub order: SortOrder,
}

impl SortInput for BookSortInput {
    fn field_name_as_string(&self) -> String {
        self.field_name.to_string()
    }
    fn order(&self) -> SortOrder {
        self.order.clone()
    }
}
#[derive(Debug, Serialize, Deserialize)]
pub struct BookFilterInput {
    pub field_name: BookFieldName,
    pub operation: FilterOperation,
    pub value: FilterValue,
}
impl FilterInput for BookFilterInput {
    fn field_name_as_string(&self) -> String {
        self.field_name.to_string()
    }
    fn operation(&self) -> FilterOperation {
        self.operation.clone()
    }
    fn value(&self) -> FilterValue {
        self.value.clone()
    }
}

#[derive(Default, Debug, Serialize, Deserialize)]
pub struct FindAllBookInput {
    /// Text search , not implement now
    pub text_search: Option<String>,
    pub filter: Option<Vec<BookFilterInput>>,
    pub pagination: Option<PaginationInput>,
    pub sort: Option<Vec<BookSortInput>>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct FindAllBookResult {
    pub items: Vec<Book>,
    pub pagination: PaginationResult,
}
