use crate::crud::Error as CrudError;
use lib_utils::uuid::Uuid;
use serde::{Deserialize, Serialize};

pub type Result<T> = core::result::Result<T, Error>;

#[derive(Debug, Serialize, Deserialize)]
pub enum Error {
    DatabaseError(String),
    BookNotFound(Uuid),
    BookTitleTooLong { actual_len: i32, max_len: i32 },
    // -- External Modules
    CrudError(CrudError),
}

// region:      ======= Error Boilerplate =======
impl core::fmt::Display for Error {
    fn fmt(
        &self,
        fmt: &mut core::fmt::Formatter,
    ) -> core::result::Result<(), core::fmt::Error> {
        write!(fmt, "{self:?}")
    }
}

impl std::error::Error for Error {}

impl From<CrudError> for Error {
    fn from(error: CrudError) -> Self {
        Error::CrudError(error)
    }
}
// region:      ======= Error Boilerplate  =======
