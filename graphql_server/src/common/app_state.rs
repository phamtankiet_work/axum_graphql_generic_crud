use lib_web::{
    configs::{FetchConfig, PostgresConfig},
    database::postgres::PostgresManager,
};
use std::env;

use crate::configs::Configs;
pub struct AppState {
    pub configs: Configs,
    pub postgres_manager: Option<PostgresManager>,
}

impl AppState {
    pub fn get_postgres_manager(&self) -> &PostgresManager {
        self.postgres_manager.as_ref().unwrap()
    }

    /// Only for unit test
    #[allow(unused)]
    pub async fn prepare_for_unit_test() -> Self {
        let configs = Configs::fetch_all().unwrap();
        env::set_var("POSTGRES_ACQUIRE_TIMEOUT", "1000");
        let postgres_config = PostgresConfig::fetch_config().unwrap();

        println!("{:?}", postgres_config);
        // Initialize database
        let postgres_manager = PostgresManager::connect(postgres_config)
            .await
            .unwrap();
        postgres_manager
            .migration()
            .await
            .unwrap();
        Self {
            configs,
            postgres_manager: Some(postgres_manager),
        }
    }
}
