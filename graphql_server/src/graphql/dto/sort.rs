use async_graphql::Enum;
use serde::{Deserialize, Serialize};

#[derive(Debug, Enum, Copy, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[graphql(rename_items = "snake_case")]
pub enum SortOrder {
    Asc,
    Desc,
}

impl From<SortOrder> for lib_crud::SortOrder {
    fn from(order: SortOrder) -> Self {
        match order {
            SortOrder::Asc => Self::Asc,
            SortOrder::Desc => Self::Desc,
        }
    }
}
