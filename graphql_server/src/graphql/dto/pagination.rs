use async_graphql::{InputObject, SimpleObject};
use serde::{Deserialize, Serialize};

#[derive(Debug, Copy, Clone, Serialize, Deserialize, InputObject)]
#[graphql(rename_fields = "snake_case")]
pub struct PaginationInput {
    #[graphql(default = 1)]
    pub page: i32,
    #[graphql(default = 10)]
    pub per_page: i32,
}
impl Default for PaginationInput {
    fn default() -> Self {
        Self {
            page: 1,
            per_page: 10,
        }
    }
}

impl From<PaginationInput> for lib_crud::PaginationInput {
    fn from(input: PaginationInput) -> Self {
        lib_crud::PaginationInput {
            page: input.page,
            per_page: input.per_page,
        }
    }
}

#[derive(Debug, Serialize, Deserialize, SimpleObject)]
#[graphql(rename_fields = "snake_case")]
pub struct PaginationResult {
    pub page: i32,
    pub per_page: i32,
    pub total_result: i32,
    pub total_pages: i32,
    pub has_previous_page: bool,
    pub has_next_page: bool,
}

impl From<lib_crud::PaginationResult> for PaginationResult {
    fn from(result: lib_crud::PaginationResult) -> Self {
        Self {
            page: result.page,
            per_page: result.per_page,
            total_result: result.total_result,
            total_pages: result.total_pages,
            has_previous_page: result.has_previous_page,
            has_next_page: result.has_next_page,
        }
    }
}

impl From<PaginationResult> for lib_crud::PaginationResult {
    fn from(result: PaginationResult) -> Self {
        Self {
            page: result.page,
            per_page: result.per_page,
            total_result: result.total_result,
            total_pages: result.total_pages,
            has_previous_page: result.has_previous_page,
            has_next_page: result.has_next_page,
        }
    }
}
