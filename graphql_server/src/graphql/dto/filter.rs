use async_graphql::{Enum, InputObject};
use lib_utils::{time::OffsetDateTime, uuid::Uuid};
use serde::{Deserialize, Serialize};

#[derive(Default, Debug, Serialize, Deserialize, InputObject)]
#[graphql(rename_fields = "snake_case")]
pub struct FilterValue {
    pub uuid_value: Option<Uuid>,
    pub uuid_array_value: Option<Vec<Uuid>>,
    pub string_value: Option<String>,
    pub int_value: Option<i32>,
    pub bool_value: Option<bool>,
    pub string_array_value: Option<Vec<String>>,
    pub date_time_value: Option<OffsetDateTime>,
}

impl From<FilterValue> for lib_crud::FilterValue {
    fn from(value: FilterValue) -> Self {
        Self {
            uuid_value: value.uuid_value,
            bool_value: value.bool_value,
            date_time_value: value.date_time_value,
            int_value: value.int_value,
            string_array_value: value.string_array_value,
            string_value: value.string_value,
            uuid_array_value: value.uuid_array_value,
        }
    }
}

#[derive(Debug, Enum, Copy, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[graphql(rename_items = "snake_case")]
pub enum FilterOperation {
    /// Equal
    Eq,
    /// Not equal
    Neq,
    /// Greater than
    Gt,
    /// Greater than or equal
    Gte,
    /// Less than
    Lt,
    /// Less than or equal
    Lte,
    /// In - Select data where value of field_name equal to any of the specified string_array_value - use only with array value
    In,
    /// Not in - Select data where value of field_name not equal to any of the specified string_array_value - use only with array value
    NotIn,
    /// Contains - Result string contains the specified value - use only with string value
    Contains,
    /// Not contains - Result string does not contain the specified value - use only with string value
    NotContains,
}

impl From<FilterOperation> for lib_crud::FilterOperation {
    fn from(operation: FilterOperation) -> Self {
        match operation {
            FilterOperation::Eq => Self::Eq,
            FilterOperation::Neq => Self::Neq,
            FilterOperation::Gt => Self::Gt,
            FilterOperation::Gte => Self::Gte,
            FilterOperation::Lt => Self::Lt,
            FilterOperation::Lte => Self::Lte,
            FilterOperation::In => Self::In,
            FilterOperation::NotIn => Self::NotIn,
            FilterOperation::Contains => Self::Contains,
            FilterOperation::NotContains => Self::NotContains,
        }
    }
}
