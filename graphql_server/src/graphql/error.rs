pub trait ToGraphqlError {
    fn to_graphql_error(&self) -> async_graphql::Error;
}
