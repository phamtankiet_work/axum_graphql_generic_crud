use axum::Extension;
use lib_web::database::postgres::PostgresManager;
use std::{sync::Arc, time::Duration};
use tokio::signal;
use tower_http::cors::{Any, CorsLayer};
use tower_http::{
    limit::RequestBodyLimitLayer, timeout::TimeoutLayer, trace::TraceLayer,
};
// use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt};

mod common;
mod configs;
mod graphql;
mod modules;
mod routes;
use common::*;
use configs::Configs;

#[tokio::main(flavor = "multi_thread")]
async fn main() {
    let configs = Configs::fetch_all().unwrap();

    // Initialize database
    let postgres_manager = PostgresManager::connect(configs.postgres.clone())
        .await
        .unwrap();
    postgres_manager
        .migration()
        .await
        .unwrap();

    // // Enable tracing.
    // tracing_subscriber::registry()
    //     .with(
    //         tracing_subscriber::EnvFilter::try_from_default_env().unwrap_or_else(|_| {
    //             "example_graceful_shutdown=debug,tower_http=debug,axum=trace".into()
    //         }),
    //     )
    //     .with(tracing_subscriber::fmt::layer().without_time())
    //     .init();

    let cors = CorsLayer::new().allow_origin(Any);

    println!("⏳ Service starting");

    // Create a regular axum app.
    let app = routes::router::create_router(Arc::new(AppState {
        configs,
        postgres_manager: Some(postgres_manager),
    }))
    // Graceful shutdown will wait for outstanding requests to complete. Add a timeout so
    // requests don't hang forever.
    .layer(Extension(TimeoutLayer::new(Duration::from_secs(10))))
    // don't allow request bodies larger than 1024 bytes, returning 413 status code
    .layer(Extension(RequestBodyLimitLayer::new(1024)))
    .layer(cors)
    .layer(Extension(TraceLayer::new_for_http()));

    let address = "0.0.0.0:8000";
    let listener = tokio::net::TcpListener::bind(address)
        .await
        .unwrap();

    println!("✅✅✅ Listening on {address}");

    // Run the server with graceful shutdown
    axum::serve(listener, app)
        .with_graceful_shutdown(shutdown_signal())
        .await
        .unwrap();
}

async fn shutdown_signal() {
    let ctrl_c = async {
        signal::ctrl_c()
            .await
            .expect("failed to install Ctrl+C handler");
    };

    #[cfg(unix)]
    let terminate = async {
        signal::unix::signal(signal::unix::SignalKind::terminate())
            .expect("failed to install signal handler")
            .recv()
            .await;
    };

    #[cfg(not(unix))]
    let terminate = std::future::pending::<()>();

    tokio::select! {
        _ = ctrl_c => {},
        _ = terminate => {},
    }
}
