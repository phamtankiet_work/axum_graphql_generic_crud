use async_graphql::{ComplexObject, Context, FieldResult};

use crate::{
    modules::{
        book::{entity::Book, service::BookService},
        chapter::entity::Chapter,
    },
    routes::graphql::get_app_state_from_ctx,
};

// region:      ======= query =======
// endregion:   ======= query =======
// region:      ======= mutation =======
// endregion:   ======= mutation =======

// region:      ======= resolver =======
// struct ChapterResolver;

// #[async_graphql::Resolver]
// impl ChapterResolver {
//     async fn book(&self, ctx: &Context<'_>, chapter: &Chapter) -> Result<Book> {
//         // Fetch the associated Book instance based on the book_id
//         let book = Book::find_by_id(chapter.book_id).await?;
//         Ok(book)
//     }
// }
#[ComplexObject]
impl Chapter {
    async fn book(&self, ctx: &Context<'_>) -> FieldResult<Book> {
        let app_state = &get_app_state_from_ctx(ctx);
        let book =
            BookService::find_one_by_id(app_state, self.book_id.unwrap()).await;
        Ok(book.unwrap())
    }
}
// endregion:   ======= resolver =======

// region:      ======= subscription =======
// endregion:   ======= subscription =======
