use crate::{modules::book::service::BookService, AppState};

use super::{entity::*, repository_postgres::ChapterRepositoryPostgres};

pub struct ChapterService {}

impl ChapterService {
    pub async fn find_all(app_state: &AppState) -> Vec<Chapter> {
        ChapterRepositoryPostgres::find_all(app_state).await
    }

    pub async fn find_one_by_id(
        app_state: &AppState,
        id: uuid::Uuid,
    ) -> Chapter {
        ChapterRepositoryPostgres::find_one_by_id(app_state, id).await
    }

    pub async fn create_one(
        app_state: &AppState,
        create_input: CreateChapterInput,
    ) -> Chapter {
        Self::check_valid_title_input(create_input.title.clone());
        Self::check_valid_book_id(app_state, create_input.book_id).await;
        let entity_to_create = Chapter {
            id: None,
            title: Some(create_input.title),
            content: Some(create_input.content),
            book_id: Some(create_input.book_id),
            created_at: None,
            updated_at: None,
        };
        ChapterRepositoryPostgres::create_one(app_state, entity_to_create).await
    }

    pub async fn update_one_by_id(
        app_state: &AppState,
        id: uuid::Uuid,
        update_input: UpdateChapterInput,
    ) -> Chapter {
        Self::check_valid_title_input(update_input.title.clone());
        let entity_to_update = Chapter {
            id: None,
            title: Some(update_input.title),
            content: Some(update_input.content),
            book_id: None,
            created_at: None,
            updated_at: None,
        };
        ChapterRepositoryPostgres::update_one_by_id(
            app_state,
            id,
            entity_to_update,
        )
        .await
    }

    pub async fn delete_one_by_id(
        app_state: &AppState,
        id: uuid::Uuid,
    ) -> Chapter {
        ChapterRepositoryPostgres::delete_one_by_id(app_state, id).await
    }
    fn check_valid_title_input(title: String) {
        if title.len() > 255 {
            panic!("title is too long");
        }
    }

    async fn check_valid_book_id(app_state: &AppState, book_id: uuid::Uuid) {
        let _book = BookService::find_one_by_id(app_state, book_id).await;
    }
}
