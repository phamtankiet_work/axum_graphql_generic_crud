use async_graphql::{Context, Object};

use crate::routes::graphql::get_app_state_from_ctx;

use super::entity::*;
use super::service::ChapterService;

// region:      ======= query =======
#[derive(Default)]
pub struct ChapterQuery;

#[Object]
impl ChapterQuery {
    async fn find_all_chapter(&self, ctx: &Context<'_>) -> Vec<Chapter> {
        let app_state = &get_app_state_from_ctx(ctx);
        ChapterService::find_all(app_state).await
    }

    async fn find_chapter_by_id(
        &self,
        ctx: &Context<'_>,
        id: uuid::Uuid,
    ) -> Chapter {
        let app_state = &get_app_state_from_ctx(ctx);
        ChapterService::find_one_by_id(app_state, id).await
    }
}
// endregion:   ======= query =======

// region:      ======= mutation =======
#[derive(Default)]
pub struct ChapterMutation;

#[Object]
impl ChapterMutation {
    async fn create_chapter(
        &self,
        ctx: &Context<'_>,
        create_chapter_input: CreateChapterInput,
    ) -> Chapter {
        let app_state = &get_app_state_from_ctx(ctx);
        ChapterService::create_one(app_state, create_chapter_input).await
    }

    async fn update_chapter(
        &self,
        ctx: &Context<'_>,
        id: uuid::Uuid,
        update_chapter_input: UpdateChapterInput,
    ) -> Chapter {
        let app_state = &get_app_state_from_ctx(ctx);
        ChapterService::update_one_by_id(app_state, id, update_chapter_input)
            .await
    }

    async fn delete_chapter(
        &self,
        ctx: &Context<'_>,
        id: uuid::Uuid,
    ) -> Chapter {
        let app_state = &get_app_state_from_ctx(ctx);
        ChapterService::delete_one_by_id(app_state, id).await
    }
}
// endregion:   ======= mutation =======

// region:      ======= resolver =======
// endregion:   ======= resolver =======

// region:      ======= subscription =======
// endregion:   ======= subscription =======
