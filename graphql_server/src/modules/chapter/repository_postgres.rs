use chrono::Utc;

use crate::database::postgres::Postgres;
use crate::AppState;

use super::entity::Chapter;

pub struct ChapterRepositoryPostgres {
    // db: Postgres,
}

// impl ChapterRepositoryPostgres {
//     pub fn new(db: Postgres) -> Self {
//         Self { db }
//     }
// }

// #[async_trait::async_trait]
impl ChapterRepositoryPostgres {
    // type Entity = Chapter;
    // type EntityId = Option<uuid::Uuid>;

    fn get_database(app_state: &AppState) -> &Postgres {
        app_state.postgres.as_ref().unwrap()
    }

    pub async fn find_all(app_state: &AppState) -> Vec<Chapter> {
        let query_result = sqlx::query_as::<_, Chapter>(
            "
                    SELECT * 
                    FROM chapters 
                    LIMIT 10
                    ",
        )
        .fetch_all(Self::get_database(app_state).pool())
        .await;

        match query_result {
            Ok(entities) => entities,
            Err(e) => {
                println!("{:?}", e);
                panic!("Something went wrong");
            }
        }
    }

    pub async fn find_one_by_id(
        app_state: &AppState,
        id: uuid::Uuid,
    ) -> Chapter {
        let query_result = sqlx::query_as::<_, Chapter>(
            "SELECT * FROM chapters WHERE id = $1",
        )
        .bind(id)
        .fetch_one(Self::get_database(app_state).pool())
        .await;

        match query_result {
            Ok(entity) => entity,
            Err(e) => {
                println!("{:?}", e);
                panic!("Something went wrong");
            }
        }
    }

    pub async fn create_one(app_state: &AppState, entity: Chapter) -> Chapter {
        let entity = Chapter::fill_data_to_save(entity);

        let query_result = sqlx::query_as::<_, Chapter>(
            r#"
            INSERT INTO chapters (id,title,content,book_id)
            VALUES ($1, $2, $3, $4)
            RETURNING *
            "#,
        )
        .bind(entity.id)
        .bind(entity.title)
        .bind(entity.content)
        .bind(entity.book_id)
        .fetch_one(Self::get_database(app_state).pool())
        .await;

        match query_result {
            Ok(entity) => entity,
            Err(e) => {
                println!("{:?}", e);
                panic!("Something went wrong");
            }
        }
    }

    pub async fn update_one_by_id(
        app_state: &AppState,
        id: uuid::Uuid,
        update_input: Chapter,
    ) -> Chapter {
        let query_result = sqlx::query_as::<_, Chapter>(
            r#"
            UPDATE chapters
            SET
                title = $3,
                content = $4,
                updated_at = $2
            WHERE id = $1
            RETURNING *
            "#,
        )
        .bind(id)
        .bind(Utc::now())
        .bind(update_input.title)
        .bind(update_input.content)
        .fetch_one(Self::get_database(app_state).pool())
        .await;

        match query_result {
            Ok(entity) => entity,
            Err(e) => {
                println!("{:?}", e);
                panic!("Something went wrong");
            }
        }
    }

    pub async fn delete_one_by_id(
        app_state: &AppState,
        id: uuid::Uuid,
    ) -> Chapter {
        let query_result = sqlx::query_as::<_, Chapter>(
            r#"
            DELETE FROM chapters
            WHERE id = $1
            RETURNING *
            "#,
        )
        .bind(id)
        .fetch_one(Self::get_database(app_state).pool())
        .await;

        match query_result {
            Ok(entity) => entity,
            Err(e) => {
                println!("{:?}", e);
                panic!("Something went wrong");
            }
        }
    }
}
