use async_graphql::{InputObject, SimpleObject};
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize, SimpleObject, sqlx::FromRow)]
#[graphql(complex)]
pub struct Chapter {
    pub id: Option<uuid::Uuid>,
    pub title: Option<String>,
    pub content: Option<String>,
    pub book_id: Option<uuid::Uuid>,
    pub created_at: Option<DateTime<Utc>>,
    pub updated_at: Option<DateTime<Utc>>,
}

impl Chapter {
    pub fn fill_data_to_save(self) -> Self {
        if self.id.is_none() {
            let id = uuid::Uuid::now_v7();
            Self {
                id: Some(id),
                ..self.clone()
            }
        } else {
            self.clone()
        }
    }
}

#[derive(Debug, Serialize, Deserialize, InputObject)]
pub struct CreateChapterInput {
    pub title: String,
    pub content: String,
    pub book_id: uuid::Uuid,
}

#[derive(Debug, Serialize, Deserialize, InputObject)]
pub struct UpdateChapterInput {
    pub title: String,
    pub content: String,
}
