pub mod book;
// pub mod chapter;

// pub mod book_chapter;
// pub mod company;
// pub mod zips;

// region:      ======= graphql =======
pub use book::graphql::*;
// pub use chapter::graphql::*;

// pub use book_chapter::graphql::*;
#[derive(async_graphql::MergedObject, Default)]
pub struct ModulesQuery(BookQuery);

#[derive(async_graphql::MergedObject, Default)]
pub struct ModulesMutation(BookMutation);
// endregion:   ======= grpahql =======
