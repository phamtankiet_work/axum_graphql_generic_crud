use async_graphql::{Context, Object};
use lib_utils::uuid::Uuid;
use lib_web::modules::book::service::BookService;

use crate::graphql::error::ToGraphqlError;
use crate::routes::graphql::get_app_state_from_ctx;

use super::dto::*;
// region:      ======= query =======
#[derive(Default)]
pub struct BookQuery;

#[Object]
impl BookQuery {
    async fn find_all_book(
        &self,
        ctx: &Context<'_>,
        find_all_book_input: FindAllBookInput,
    ) -> async_graphql::Result<FindAllBookResult> {
        let app_state = &get_app_state_from_ctx(ctx);
        let connection_manager = app_state.get_postgres_manager();
        let result = BookService::find_all(
            connection_manager,
            find_all_book_input.into(),
        )
        .await;
        match result {
            Ok(result) => async_graphql::Result::Ok(result.into()),
            Err(error) => async_graphql::Result::Err(error.to_graphql_error()),
        }
    }

    async fn find_book_by_id(
        &self,
        ctx: &Context<'_>,
        id: Uuid,
    ) -> async_graphql::Result<Book> {
        let app_state = get_app_state_from_ctx(ctx);
        let connection_manager = app_state.get_postgres_manager();
        let result = BookService::find_one_by_id(connection_manager, id).await;
        match result {
            Ok(result) => async_graphql::Result::Ok(result.into()),
            Err(error) => async_graphql::Result::Err(error.to_graphql_error()),
        }
    }
}
// endregion:   ======= query =======

// region:      ======= mutation =======
#[derive(Default)]
pub struct BookMutation;
#[Object]
impl BookMutation {
    async fn create_book(
        &self,
        ctx: &Context<'_>,
        create_book_input: CreateBookInput,
    ) -> async_graphql::Result<Book> {
        let app_state = &get_app_state_from_ctx(ctx);
        let connection_manager = app_state.get_postgres_manager();
        let result = BookService::create_one(
            connection_manager,
            create_book_input.into(),
        )
        .await;
        match result {
            Ok(result) => async_graphql::Result::Ok(result.into()),
            Err(error) => async_graphql::Result::Err(error.to_graphql_error()),
        }
    }

    async fn update_book(
        &self,
        ctx: &Context<'_>,
        id: Uuid,
        update_book_input: UpdateBookInput,
    ) -> async_graphql::Result<Book> {
        let app_state = &get_app_state_from_ctx(ctx);
        let connection_manager = app_state.get_postgres_manager();
        let result = BookService::update_one_by_id(
            connection_manager,
            id,
            update_book_input.into(),
        )
        .await;
        match result {
            Ok(result) => async_graphql::Result::Ok(result.into()),
            Err(error) => async_graphql::Result::Err(error.to_graphql_error()),
        }
    }

    async fn delete_book(
        &self,
        ctx: &Context<'_>,
        id: Uuid,
    ) -> async_graphql::Result<Book> {
        let app_state = &get_app_state_from_ctx(ctx);
        let connection_manager = app_state.get_postgres_manager();
        let result =
            BookService::delete_one_by_id(connection_manager, id).await;
        match result {
            Ok(result) => async_graphql::Result::Ok(result.into()),
            Err(error) => async_graphql::Result::Err(error.to_graphql_error()),
        }
    }
}
// endregion:   ======= mutation =======

// region:      ======= resolver =======
// endregion:   ======= resolver =======

// region:      ======= subscription =======
// endregion:   ======= subscription =======
