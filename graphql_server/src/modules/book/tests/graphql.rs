use async_graphql::value;

use crate::modules::book::dto::Book;

use super::*;

// region:          ======= create_one =======
#[tokio::test]
async fn create_one_ok() {
    let schema = common::prepare_schema().await;

    let query = r#"
    mutation {
        createBook(createBookInput: {
            title: "book_name"
        })  {
            id
            title
            created_at
            updated_at
        }
    }
    "#;

    let response = schema
        .execute(query)
        .await
        .into_result()
        .unwrap()
        .data;

    // let book = Book {
    //     id: None,
    //     title: Some("book_name".to_string()),
    //     created_at: None,
    //     updated_at: None,
    // };
    let book = response
    let expected_response = value!({ "createBook": {
        // "id" : _,
        "name": "book_name",
        // "created_at": "",
        // "updated_at": "",
    }});
    println!("value {:?}", response.clone().into_value());
    println!("json {:?}", response.clone().into_json());
    println!("string {:?}", response.to_string());
    assert_eq!(response, expected_response);
    assert!(true);
}
// endregion:       ======= create_one =======
