use std::sync::Arc;

use async_graphql::{EmptySubscription, Schema};

use crate::AppState;

use super::*;

type BookSchema = Schema<BookQuery, BookMutation, EmptySubscription>;

pub async fn prepare_schema() -> BookSchema {
    let app_state = AppState::prepare_for_unit_test().await;

    Schema::build(BookQuery, BookMutation, EmptySubscription)
        .data(Arc::new(app_state))
        .finish()
}
