use async_graphql::{Description, Enum, InputObject, SimpleObject};
use lib_utils::{time::OffsetDateTime, uuid::Uuid};
use lib_web::modules::book::entity;

use serde::{Deserialize, Serialize};

use crate::graphql::dto::*;

#[derive(Clone, Debug, Serialize, Deserialize, SimpleObject)]
#[graphql(rename_fields = "snake_case")]
pub struct Book {
    pub id: Option<Uuid>,
    pub title: Option<String>,
    pub created_at: Option<OffsetDateTime>,
    pub updated_at: Option<OffsetDateTime>,
}

impl From<entity::Book> for Book {
    fn from(book: entity::Book) -> Self {
        Self {
            id: book.id,
            title: book.title,
            created_at: book.created_at,
            updated_at: book.updated_at,
        }
    }
}

impl From<Book> for entity::Book {
    fn from(book: Book) -> Self {
        Self {
            id: book.id,
            title: book.title,
            created_at: book.created_at,
            updated_at: book.updated_at,
        }
    }
}

#[derive(Debug, Serialize, Deserialize, InputObject)]
pub struct CreateBookInput {
    pub title: String,
}

impl From<CreateBookInput> for entity::CreateBookInput {
    fn from(input: CreateBookInput) -> Self {
        Self { title: input.title }
    }
}

#[derive(Debug, Serialize, Deserialize, InputObject)]
pub struct UpdateBookInput {
    pub title: String,
}

impl From<UpdateBookInput> for entity::UpdateBookInput {
    fn from(input: UpdateBookInput) -> Self {
        Self { title: input.title }
    }
}

#[derive(Debug, Enum, Copy, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[graphql(rename_items = "snake_case")]
pub enum BookFieldName {
    Id,
    Title,
    CreatedAt,
    UpdatedAt,
}

impl From<BookFieldName> for entity::BookFieldName {
    fn from(field_name: BookFieldName) -> Self {
        match field_name {
            BookFieldName::Id => Self::Id,
            BookFieldName::Title => Self::Title,
            BookFieldName::CreatedAt => Self::CreatedAt,
            BookFieldName::UpdatedAt => Self::UpdatedAt,
        }
    }
}

/// This sorts everything by field name with order first,
/// and then by next array value field name and order
/// whenever the field value of field name before for two or more rows are equal.
#[derive(Description, Debug, Serialize, Deserialize, InputObject)]
#[graphql(rename_fields = "snake_case")]
pub struct BookSortInput {
    pub field_name: BookFieldName,
    pub order: SortOrder,
}

impl From<BookSortInput> for entity::BookSortInput {
    fn from(input: BookSortInput) -> Self {
        Self {
            field_name: input.field_name.into(),
            order: input.order.into(),
        }
    }
}

#[derive(Debug, Serialize, Deserialize, async_graphql::InputObject)]
#[graphql(rename_fields = "snake_case")]
pub struct BookFilterInput {
    pub field_name: BookFieldName,
    pub operation: FilterOperation,
    pub value: FilterValue,
}

impl From<BookFilterInput> for entity::BookFilterInput {
    fn from(input: BookFilterInput) -> Self {
        Self {
            field_name: input.field_name.into(),
            operation: input.operation.into(),
            value: input.value.into(),
        }
    }
}

#[derive(Default, Debug, Serialize, Deserialize, InputObject)]
#[graphql(rename_fields = "snake_case")]
pub struct FindAllBookInput {
    /// Text search , not implement now
    pub text_search: Option<String>,
    pub filter: Option<Vec<BookFilterInput>>,
    pub pagination: Option<PaginationInput>,
    pub sort: Option<Vec<BookSortInput>>,
}

impl From<FindAllBookInput> for entity::FindAllBookInput {
    fn from(input: FindAllBookInput) -> Self {
        Self {
            text_search: input.text_search,
            filter: input.filter.map(|vec_a| {
                vec_a
                    .into_iter()
                    .map(|a| a.into())
                    .collect()
            }),
            pagination: input.pagination.map(|a| a.into()),
            sort: input.sort.map(|vec_a| {
                vec_a
                    .into_iter()
                    .map(|a| a.into())
                    .collect()
            }),
        }
    }
}

#[derive(Debug, Serialize, Deserialize, SimpleObject)]
#[graphql(rename_fields = "snake_case")]
pub struct FindAllBookResult {
    pub items: Vec<Book>,
    pub pagination: PaginationResult,
}

impl From<FindAllBookResult> for entity::FindAllBookResult {
    fn from(result: FindAllBookResult) -> Self {
        Self {
            items: result
                .items
                .into_iter()
                .map(|a| a.into())
                .collect(),
            pagination: result.pagination.into(),
        }
    }
}

impl From<entity::FindAllBookResult> for FindAllBookResult {
    fn from(value: entity::FindAllBookResult) -> Self {
        Self {
            items: value
                .items
                .into_iter()
                .map(|a| a.into())
                .collect(),
            pagination: value.pagination.into(),
        }
    }
}
