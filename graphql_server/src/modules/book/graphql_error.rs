use crate::graphql::error::ToGraphqlError;
use async_graphql::ErrorExtensions;
use lib_web::modules::book::Error;

impl ToGraphqlError for Error {
    // convert error to graphql error, and set code. code is for frontend to handle different error.
    fn to_graphql_error(&self) -> async_graphql::Error {
        match self {
            Error::BookNotFound(_) => {
                self.extend_with(|_, e| e.set("code", "BOOK_NOT_FOUND"))
            }
            Error::BookTitleTooLong { .. } => {
                self.extend_with(|_, e| e.set("code", "BOOK_TITLE_TOO_LONG"))
            }
            // all unhandle internal error must redact before send to frontend
            _ => {
                let new_err = async_graphql::Error::new("");
                new_err.extend_with(|_, e| e.set("code", "UNKNOWN_ERROR"))
            }
        }
    }
}
