use lib_web::configs::{FetchConfig, PostgresConfig};

use super::app::AppConfig;
use super::Result;

pub struct Configs {
    pub app: AppConfig,
    pub postgres: PostgresConfig,
}

impl Configs {
    pub fn fetch_all() -> Result<Configs> {
        Ok(Configs {
            app: AppConfig::fetch_config()?,
            postgres: PostgresConfig::fetch_config()?,
        })
    }
}

impl Configs {
    pub fn app(&self) -> &AppConfig {
        &self.app
    }
}
