use lib_web::configs::FetchConfig;

use super::{try_get_env, Result};

pub struct AppConfig {
    pub name: String,
    pub version: String,
    pub description: Option<String>,
}

impl FetchConfig for AppConfig {
    fn fetch_config() -> Result<Self> {
        Ok(Self {
            name: try_get_env("CARGO_PKG_NAME")?,
            version: try_get_env("CARGO_PKG_VERSION")?,
            description: try_get_env("CARGO_PKG_DESCRIPTION").ok(),
        })
    }
}
