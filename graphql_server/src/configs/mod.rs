use lib_utils::env::try_get_env;

mod app;
mod config;

pub use config::Configs;
pub use lib_web::configs::Result;
