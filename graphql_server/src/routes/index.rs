use std::sync::Arc;

use axum::{
    extract::State, http::StatusCode, response::IntoResponse, routing::get,
    Json, Router,
};
use serde::Serialize;

use crate::AppState;

pub fn routes(app_state: Arc<AppState>) -> Router {
    Router::new()
        .route("/", get(index_handler))
        .with_state(app_state)
}

#[derive(Serialize)]
pub struct IndexInfo {
    pub name: String,
    pub version: String,
    pub description: Option<String>,
}

async fn index_handler(
    State(app_state): State<Arc<AppState>>,
) -> impl IntoResponse {
    let app_config = app_state.configs.app();
    (
        StatusCode::OK,
        Json(IndexInfo {
            name: app_config.name.clone(),
            version: app_config.version.clone(),
            description: app_config.description.clone(),
        }),
    )
}
