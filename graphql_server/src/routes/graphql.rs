use async_graphql::http::{playground_source, GraphQLPlaygroundConfig};
use async_graphql::{Context, Object, Schema};
use async_graphql::{EmptyMutation, EmptySubscription};
use async_graphql_axum::{GraphQLRequest, GraphQLResponse};
use axum::{
    extract::Extension,
    response::{Html, IntoResponse},
    routing::get,
    Router,
};
use opentelemetry::trace::TraceContextExt;
use std::sync::Arc;
use tracing::{info, span, Instrument, Level};
use tracing_opentelemetry::OpenTelemetrySpanExt;

use crate::modules::ModulesMutation;
use crate::modules::ModulesQuery;
use crate::AppState;

#[derive(Default)]
pub(crate) struct QueryRoot;

#[Object]
impl QueryRoot {
    async fn hello(&self, _ctx: &Context<'_>) -> &'static str {
        "Hello world"
    }
}

#[derive(async_graphql::MergedObject, Default)]
pub struct Query(QueryRoot, ModulesQuery);

#[derive(async_graphql::MergedObject, Default)]
pub struct Mutation(EmptyMutation, ModulesMutation);

pub type AppSchema = Schema<Query, Mutation, EmptySubscription>;

pub fn routes(app_state: Arc<AppState>) -> Router {
    let schema =
        Schema::build(Query::default(), Mutation::default(), EmptySubscription)
            .data(app_state.clone())
            .finish();

    Router::new()
        .route(
            "/api/graphql",
            get(graphql_playground).post(graphql_handler),
        )
        .layer(Extension(schema))
}

async fn graphql_playground() -> impl IntoResponse {
    Html(playground_source(
        GraphQLPlaygroundConfig::new("/api/graphql")
            .subscription_endpoint("/api/graphql/ws"),
    ))
}

async fn graphql_handler(
    schema: Extension<AppSchema>,
    req: GraphQLRequest,
) -> GraphQLResponse {
    let span = span!(Level::INFO, "graphql_execution");

    info!("Processing GraphQL request");

    let response = async move { schema.execute(req.into_inner()).await }
        .instrument(span.clone())
        .await;

    info!("Processing GraphQL request finished");

    response
        .extension(
            "traceId",
            async_graphql::Value::String(format!(
                "{}",
                span.context().span().span_context().trace_id()
            )),
        )
        .into()
}

pub fn get_app_state_from_ctx(ctx: &Context<'_>) -> Arc<AppState> {
    ctx.data::<Arc<AppState>>()
        .expect("Failed to get app state")
        .clone()
}
