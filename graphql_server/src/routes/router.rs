use std::sync::Arc;

use axum::Router;

use crate::{
    routes::{fallback, graphql, health, index},
    AppState,
};

pub fn create_router(app_state: Arc<AppState>) -> Router {
    Router::new()
        .merge(index::routes(app_state.clone()))
        .merge(health::routes())
        .merge(graphql::routes(app_state.clone()))
        .fallback(fallback::page_not_found_handler)
}
