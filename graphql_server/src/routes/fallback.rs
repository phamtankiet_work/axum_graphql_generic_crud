use axum::{http::StatusCode, response::IntoResponse};

pub async fn page_not_found_handler() -> impl IntoResponse {
    (StatusCode::NOT_FOUND, "404 not found")
}
