use axum::{http::StatusCode, response::IntoResponse, Json};
use axum::{routing::get, Router};
use serde::Serialize;

#[derive(Serialize)]
struct Health {
    healthy: bool,
}

pub fn routes() -> Router {
    Router::new().route("/health", get(health_check_handler))
}

async fn health_check_handler() -> impl IntoResponse {
    let health = Health { healthy: true };

    (StatusCode::OK, Json(health))
}
