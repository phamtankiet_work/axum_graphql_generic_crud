# install

```
cargo install cargo-watch
```
# command
```
cargo watch -c -w src/ -x "run"
```

# test
```
cargo watch -c -w src/ -x "test"
```

# test - only one
```
cargo watch -c -w src/ -x "test --package graphql_server --bin graphql_server -- modules::book::tests::repository_postgres::prepare_find_all_query --exact --show-output"
```

# migration add
```
cargo sqlx migrate add <name>
```

# migration up
```
cargo sqlx migrate run
```

# migration down
```
cargo sqlx migrate revert
```