https://rust-lang.github.io/api-guidelines/naming.html

Casing conforms to RFC 430 (C-CASE)
Basic Rust naming conventions are described in RFC 430.

In general, Rust tends to use UpperCamelCase for "type-level" constructs (types and traits) and snake_case for "value-level" constructs. More precisely:

| Item                    | Convention                                                 |
| :---------------------- | :--------------------------------------------------------- |
| Crates                  | unclear                                                    |
| Modules                 | snake_case                                                 |
| Types                   | UpperCamelCase                                             |
| Traits                  | UpperCamelCase                                             |
| Enum variants           | UpperCamelCase                                             |
| Functions               | snake_case                                                 |
| Methods                 | snake_case                                                 |
| General constructors    | new or with_more_details                                   |
| Conversion constructors | from_some_other_type                                       |
| Macros                  | snake_case!                                                |
| Local variables         | snake_case                                                 |
| Statics                 | SCREAMING_SNAKE_CASE                                       |
| Constants               | SCREAMING_SNAKE_CASE                                       |
| Type parameters         | concise UpperCamelCase, usually single uppercase letter: T |
| Lifetimes               | short lowercase, usually a single letter: 'a, 'de, 'src    |
| Features                | unclear but see C-FEATURE                                  |


CONV
Conversions should be provided as methods, with names prefixed as follows:

| Prefix | Cost      | Ownership                                   |
| ------ | --------- | ------------------------------------------- |
| as_    | Free      | borrowed -> borrowed                        |
| ------ | --------- | ------------------------------------------- |
| to_    | Expensive | borrowed -> borrowed                        |
|        |           | borrowed  ->         owned (non-Copy types) |
|        |           | owned     ->         owned (Copy types)     |
| ------ | --------- | ------------------------------------------- |
| into_  | Variable  | owned -> owned (non-Copy types)             |

